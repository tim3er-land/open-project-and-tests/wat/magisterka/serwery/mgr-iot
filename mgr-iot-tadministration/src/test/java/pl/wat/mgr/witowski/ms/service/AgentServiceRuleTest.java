package pl.wat.mgr.witowski.ms.service;

import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import pl.wat.mgr.witowski.ms.commons.dictionary.DictionaryCacheService;
import pl.wat.mgr.witowski.ms.database.repository.IotAgentEntityRepository;
import pl.wat.mgr.witowski.ms.database.repository.IotAgentRuleRepository;
import pl.wat.mgr.witowski.ms.database.repository.IotAgentTopicRepository;
import pl.wat.mgr.witowski.ms.database.repository.IotAgentUserRepository;

public class AgentServiceRuleTest {
    private IotAgentEntityRepository iotAgentEntityRepository;
    private IotAgentUserRepository iotAgentUserRepository;
    private IotAgentTopicRepository iotAgentTopicRepository;
    private IotAgentRuleRepository iotAgentRuleRepository;
    private DictionaryCacheService dictionaryCacheService;
    private FileService fileService;
    private KeysServices keysServices;
    private AgentService agentService;

    @BeforeEach
    void setUp() {
        iotAgentEntityRepository = Mockito.mock(IotAgentEntityRepository.class);
        iotAgentUserRepository = Mockito.mock(IotAgentUserRepository.class);
        iotAgentTopicRepository = Mockito.mock(IotAgentTopicRepository.class);
        iotAgentRuleRepository = Mockito.mock(IotAgentRuleRepository.class);
        dictionaryCacheService = Mockito.mock(DictionaryCacheService.class);
        fileService = Mockito.mock(FileService.class);
        keysServices = Mockito.mock(KeysServices.class);
        agentService = new AgentService(iotAgentEntityRepository, iotAgentUserRepository, iotAgentTopicRepository, iotAgentRuleRepository, dictionaryCacheService, fileService, keysServices);
    }
}
