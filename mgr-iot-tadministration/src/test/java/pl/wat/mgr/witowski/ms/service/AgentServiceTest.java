package pl.wat.mgr.witowski.ms.service;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import pl.wat.mgr.witowski.ms.commons.dictionary.DictionaryCacheService;
import pl.wat.mgr.witowski.ms.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.ms.database.dao.IotAgentEntity;
import pl.wat.mgr.witowski.ms.database.dao.IotAgentRuleEntity;
import pl.wat.mgr.witowski.ms.database.dao.IotAgentTopicEntity;
import pl.wat.mgr.witowski.ms.database.repository.IotAgentEntityRepository;
import pl.wat.mgr.witowski.ms.database.repository.IotAgentRuleRepository;
import pl.wat.mgr.witowski.ms.database.repository.IotAgentTopicRepository;
import pl.wat.mgr.witowski.ms.database.repository.IotAgentUserRepository;
import pl.wat.mgr.witowski.ms.dto.AgentTopic;
import pl.wat.mgr.witowski.ms.dto.rest.AgentExtDto;
import pl.wat.mgr.witowski.ms.dto.rest.BasicAgentDto;
import pl.wat.mgr.witowski.ms.dto.security.FullKeyDto;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class AgentServiceTest {

    private IotAgentEntityRepository iotAgentEntityRepository;
    private IotAgentUserRepository iotAgentUserRepository;
    private IotAgentTopicRepository iotAgentTopicRepository;
    private IotAgentRuleRepository iotAgentRuleRepository;
    private DictionaryCacheService dictionaryCacheService;
    private FileService fileService;
    private KeysServices keysServices;
    private AgentService agentService;

    @BeforeEach
    void setUp() {
        iotAgentEntityRepository = Mockito.mock(IotAgentEntityRepository.class);
        iotAgentUserRepository = Mockito.mock(IotAgentUserRepository.class);
        iotAgentTopicRepository = Mockito.mock(IotAgentTopicRepository.class);
        iotAgentRuleRepository = Mockito.mock(IotAgentRuleRepository.class);
        dictionaryCacheService = Mockito.mock(DictionaryCacheService.class);
        fileService = Mockito.mock(FileService.class);
        keysServices = Mockito.mock(KeysServices.class);
        agentService = new AgentService(iotAgentEntityRepository, iotAgentUserRepository, iotAgentTopicRepository, iotAgentRuleRepository, dictionaryCacheService, fileService, keysServices);
    }

    @Test
    void addNewAgentValidationTest() {
        try {
            agentService.addNewAgent(BasicAgentDto.builder().build());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-400-001-001");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-400-001-001");
            Assert.assertTrue(ap.getErrorDtos().stream().filter(errorDto -> errorDto.getCode().equalsIgnoreCase("mgr-iot-administration-001-001")).findFirst().isPresent());
        }
    }

    @Test
    void addNewAgentExceptionTest() {
        try {
            when(iotAgentEntityRepository.save(any())).thenThrow(new NullPointerException());
            agentService.addNewAgent(BasicAgentDto.builder().agentName("agentName").build());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-500-001-002");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-internal-error");
            Assert.assertEquals(ap.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Test
    void addNewAgentSuccesfullTest() {
        BasicAgentDto agentDto = BasicAgentDto.builder().agentName("agentName").build();
        IotAgentEntity iotAgent = new IotAgentEntity();
        iotAgent.setIotAgtUid(UUID.randomUUID().toString());
        iotAgent.setIotAgtName(agentDto.getAgentName());
        when(iotAgentEntityRepository.save(any())).thenReturn(iotAgent);
        AgentExtDto agentExtDto = agentService.addNewAgent(agentDto);

        Assert.assertNotNull(agentExtDto.getAgentUid());
        Assert.assertNotNull(agentDto.getAgentName());
        Assert.assertEquals(agentDto.getAgentName(), agentDto.getAgentName());
    }


    @Test
    void getAgentZipNotFoundAgentTest() {
        try {
            when(iotAgentEntityRepository.findAgentByUid(any(), any())).thenReturn(null);
            agentService.getAgentZip(UUID.randomUUID().toString());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-400-002-002");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-400-002-002");
            Assert.assertEquals(ap.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void getAgentZipExceptionTest() {
        try {
            when(iotAgentEntityRepository.findAgentByUid(any(), any())).thenReturn(new IotAgentEntity());
            agentService.getAgentZip(UUID.randomUUID().toString());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-500-002-001");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-internal-error");
            Assert.assertEquals(ap.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Test
    void getAgentZipSuccesFullTest() {
        when(keysServices.generateAgentNewKeys()).thenReturn(new FullKeyDto());
        when(iotAgentEntityRepository.findAgentByUid(any(), any())).thenReturn(new IotAgentEntity());
        when(fileService.prepateZipFile(any(), any(), any(), any())).thenReturn(new File("test"));
        File agentZip = agentService.getAgentZip(UUID.randomUUID().toString());
        Assert.assertNotNull(agentZip);
    }

    @Test
    void getUserIotAgentsExceptionTest() {
        try {
            when(iotAgentEntityRepository.getUserIotAgents(any())).thenThrow(new NullPointerException());
            agentService.getUserIotAgents();
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-500-003-001");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-internal-error");
            Assert.assertEquals(ap.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Test
    void getUserIotAgentsNotFoundTest() {
        when(iotAgentEntityRepository.getUserIotAgents(any())).thenReturn(Collections.emptyList());
        List<AgentExtDto> userIotAgents = agentService.getUserIotAgents();
        Assert.assertNull(userIotAgents);
    }

    @Test
    void deleteUserAgentNotFoundAgentTest() {
        try {
            when(iotAgentEntityRepository.getUserAgentBasedOnUid(any(), any())).thenReturn(null);
            agentService.deleteUserAgent(UUID.randomUUID().toString());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-400-003-001");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-400-003-001");
            Assert.assertEquals(ap.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void deleteUserAgentExceptionTest() {
        try {
            when(iotAgentEntityRepository.getUserAgentBasedOnUid(any(), any())).thenThrow(new NullPointerException());
            agentService.deleteUserAgent(UUID.randomUUID().toString());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-500-003-002");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-internal-error");
            Assert.assertEquals(ap.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Test
    void getAgentTopicsNotFoundAgentTest() {
        try {
            when(iotAgentEntityRepository.getUserAgentBasedOnUid(any(), any())).thenReturn(null);
            List<AgentTopic> agentTopics = agentService.getAgentTopics(UUID.randomUUID().toString());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-400-004-001");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-400-004-001");
            Assert.assertEquals(ap.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void getAgentTopicsExceptionTest() {
        try {
            when(iotAgentEntityRepository.getUserAgentBasedOnUid(any(), any())).thenThrow(new NullPointerException());
            List<AgentTopic> agentTopics = agentService.getAgentTopics(UUID.randomUUID().toString());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-500-004-002");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-internal-error");
            Assert.assertEquals(ap.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Test
    void getAgentTopicsNotFoundTopicTest() {
        when(iotAgentEntityRepository.getUserAgentBasedOnUid(any(), any())).thenReturn(new IotAgentEntity());
        List<AgentTopic> agentTopics = agentService.getAgentTopics(UUID.randomUUID().toString());
        Assert.assertNull(agentTopics);
    }

    @Test
    void getAgentTopicsSuccesfullTest() {
        IotAgentEntity iotAgent = new IotAgentEntity();
        iotAgent.setIotAgentTopicsByIotAgtId(Arrays.asList(new IotAgentTopicEntity(UUID.randomUUID().toString(), "/test1"), new IotAgentTopicEntity(UUID.randomUUID().toString(), "/test2")));
        when(iotAgentEntityRepository.getUserAgentBasedOnUid(any(), any())).thenReturn(iotAgent);
        List<AgentTopic> agentTopics = agentService.getAgentTopics(UUID.randomUUID().toString());
        Assert.assertNotNull(agentTopics);
        Assert.assertTrue(agentTopics.stream().filter(agentTopic -> agentTopic.getTopicConfig().equalsIgnoreCase("/test1")).findFirst().isPresent());
        Assert.assertTrue(agentTopics.stream().allMatch(agentTopic -> StringUtils.hasText(agentTopic.getTopicConfig()) && StringUtils.hasText(agentTopic.getTopicUid())));
    }

    @Test
    void deleteAgentRuleExceptionTest() {
        try {
            when(iotAgentRuleRepository.getUserRule(any(), any())).thenThrow(new NullPointerException());
            agentService.deleteAgentRule(UUID.randomUUID().toString());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-500-007-003");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-internal-error");
            Assert.assertEquals(ap.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Test
    void deleteAgentRuleNotFoundTest() {
        try {
            when(iotAgentRuleRepository.getUserRule(any(), any())).thenReturn(null);
            agentService.deleteAgentRule(UUID.randomUUID().toString());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-400-007-001");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-400-007-001");
            Assert.assertEquals(ap.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void deleteAgentRuleSuccesfullTest() {
        when(iotAgentRuleRepository.getUserRule(any(), any())).thenReturn(new IotAgentRuleEntity());
        agentService.deleteAgentRule(UUID.randomUUID().toString());
    }

    @Test
    void addAgentTopicExceptionTest() {
        try {
            when(iotAgentEntityRepository.getUserAgentBasedOnUid(any())).thenThrow(new NullPointerException());
            agentService.addAgentTopic(UUID.randomUUID().toString(), Arrays.asList("/test"));
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-500-008-002");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-internal-error");
            Assert.assertEquals(ap.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Test
    void addAgentTopicNotFoundTest() {
        try {
            when(iotAgentEntityRepository.getUserAgentBasedOnUid(any())).thenReturn(null);
            agentService.addAgentTopic(UUID.randomUUID().toString(), Arrays.asList("/test"));
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-400-008-001");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-400-008-001");
            Assert.assertEquals(ap.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void addAgentTopicSuccesfullTest() {
        when(iotAgentEntityRepository.getUserAgentBasedOnUid(any())).thenReturn(new IotAgentEntity());
        agentService.addAgentTopic(UUID.randomUUID().toString(), Arrays.asList("/test"));
    }

    @Test
    void deleteAgentTopicExceptionTest() {
        try {
            when(iotAgentEntityRepository.getUserAgentBasedOnUid(any())).thenThrow(new NullPointerException());
            agentService.deleteAgentTopic(UUID.randomUUID().toString());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-500-010-002");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-internal-error");
            Assert.assertEquals(ap.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Test
    void deleteAgentTopicNotFoundTest() {
        try {
            when(iotAgentEntityRepository.getUserAgentBasedOnUid(any())).thenReturn(null);
            agentService.deleteAgentTopic(UUID.randomUUID().toString());
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getCode(), "mgr-iot-administration-400-010-001");
            Assert.assertEquals(ap.getDescription(), "mgr-iot-administration-400-010-001");
            Assert.assertEquals(ap.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    void deleteAgentTopicSuccesfullTest() {
        when(iotAgentEntityRepository.getUserAgentBasedOnUid(any())).thenReturn(new IotAgentEntity());
        agentService.deleteAgentTopic(UUID.randomUUID().toString());
    }



}
