package pl.wat.mgr.witowski.ms.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentRule {
    private String ruleUid;
    private String ruleCode;
    private String ruleConfig;
}
