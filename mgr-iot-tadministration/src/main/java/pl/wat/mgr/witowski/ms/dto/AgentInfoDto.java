package pl.wat.mgr.witowski.ms.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentInfoDto {
    private String agentName;
    private List<String> agentTopics;
    private String agentUid;
    private String agentModifyDate;
}
