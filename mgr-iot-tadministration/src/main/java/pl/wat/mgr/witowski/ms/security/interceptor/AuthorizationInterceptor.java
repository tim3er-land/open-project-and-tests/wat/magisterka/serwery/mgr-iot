package pl.wat.mgr.witowski.ms.security.interceptor;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import pl.wat.mgr.witowski.ms.security.jwt.KeyServiceUtils;

import java.io.IOException;

public class AuthorizationInterceptor implements ClientHttpRequestInterceptor {
    public AuthorizationInterceptor() {
    }

    public ClientHttpResponse intercept(HttpRequest hr, byte[] bytes, ClientHttpRequestExecution chre) throws IOException {
        HttpHeaders headers = hr.getHeaders();
        headers.add("Authorization", "Bearer " + KeyServiceUtils.getMicroToken());
        return chre.execute(hr, bytes);
    }
}
