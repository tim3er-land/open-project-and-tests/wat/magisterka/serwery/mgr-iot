package pl.wat.mgr.witowski.ms.commons.rest;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.util.ContentCachingResponseWrapper;
import pl.wat.mgr.witowski.ms.commons.rest.wrappers.RequestWrapper;
import pl.wat.mgr.witowski.ms.security.jwt.JwtUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Component
public class RequestResponseLoggingFilter implements Filter {
    private Gson  gson;

    @Value("${pl.wat.mgr.witowski.ms.name:unknown}")
    private String msName;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        gson = new Gson();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        Long duractionStart = Calendar.getInstance().getTimeInMillis();
        RestApiLog.RestApiLogBuilder builder = RestApiLog.builder();

        ContentCachingResponseWrapper responseCacheWrapperObject = new ContentCachingResponseWrapper((HttpServletResponse) servletResponse);
        OneWayRestApiLog requestLog = null;
        String path = null;
        String remoteAdress = null;
        String userUid = null;
        String userLogin = null;
        try {
            if (StringUtils.hasText(((HttpServletRequest) servletRequest).getHeader("Authorization"))) {
                String authorization = ((HttpServletRequest) servletRequest).getHeader("Authorization");
                authorization = authorization.replace("Bearer ", "");
                userUid = JwtUtils.getUUID(authorization);
                userLogin = JwtUtils.getUsername(authorization);
            }
        } catch (Exception ex) {
            log.error("Error occured extract user uid", ex);
        }
        if (((HttpServletRequest) servletRequest).getHeader("Content-Type") != null && ((HttpServletRequest) servletRequest).getHeader("Content-Type").contains("multipart/form-data")) {
            filterChain.doFilter(servletRequest, responseCacheWrapperObject);
        } else {
            RequestWrapper requestWrapper = new RequestWrapper((HttpServletRequest) servletRequest);
            filterChain.doFilter(requestWrapper, responseCacheWrapperObject);
            path = requestWrapper.getRequestURI();
            remoteAdress = requestWrapper.getRemoteAddr();
            requestLog = requestWrapper.getRequestLog();
        }
        // make rest api

        WebApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletRequest.getServletContext());
        HandlerMapping handlerMappings = (HandlerMapping) BeanFactoryUtils.beansOfTypeIncludingAncestors(applicationContext, HandlerMapping.class, true, false).get("requestMappingHandlerMapping");
        String opName = null;

        try {
            opName = ((HandlerMethod) handlerMappings.getHandler((HttpServletRequest) servletRequest).getHandler()).getMethod().getName();
        } catch (Exception ex) {
            opName = "unknown";
            log.error("No method found for path in handler", ex);
        }


        //responce log
        HttpServletResponseWrapper httpServletResponseWrapper = new HttpServletResponseWrapper((HttpServletResponse) servletResponse);
        byte[] responseArray = responseCacheWrapperObject.getContentAsByteArray();
        String responseStr = new String(responseArray, responseCacheWrapperObject.getCharacterEncoding());
        responseCacheWrapperObject.copyBodyToResponse();
        //rest log
        builder.status(httpServletResponseWrapper.getStatus())
                .statusCode(HttpStatus.resolve(httpServletResponseWrapper.getStatus()))
                .requestLog(requestLog)
                .responceLog(prepareResponceLog(responseStr, httpServletResponseWrapper))
                .duraction(calculationDuraction(duractionStart))
                .path(path)
                .op(opName)
                .ms(msName)
                .userUid(userUid)
                .method(((HttpServletRequest) servletRequest).getMethod())
                .remoteAdress(remoteAdress);

        //log log
        logRestApi(builder.build());
    }

    private OneWayRestApiLog prepareResponceLog(String responseStr, HttpServletResponseWrapper httpServletResponseWrapper) {
        return OneWayRestApiLog.builder()
                .headers(prepareHeaders(httpServletResponseWrapper))
                .body(prepareBody(responseStr))
                .lenght(StringUtils.hasText(responseStr) ? responseStr.length() : 0)
                .build();
    }

    private Object prepareBody(String responseStr) {
        if (!StringUtils.hasText(responseStr)) {
            if (responseStr.startsWith("[")) {
                return gson.fromJson(responseStr, new TypeToken<ArrayList<Map<String, Object>>>() {
                }.getType());
            }
            return gson.fromJson(responseStr, HashMap.class);
        }
        return null;
    }

    private List<HashMapLog> prepareHeaders(HttpServletResponseWrapper httpServletResponseWrapper) {
        return httpServletResponseWrapper.getHeaderNames().stream().map(s -> {
            return HashMapLog.builder()
                    .name(s)
                    .value(httpServletResponseWrapper.getHeader(s))
                    .build();
        }).collect(Collectors.toList());
    }

    private Long calculationDuraction(Long duractionStart) {
        return Math.abs(Calendar.getInstance().getTimeInMillis() - duractionStart);
    }


    private void logRestApi(RestApiLog restApiLog) {
        try {
            if (restApiLog.getStatus() < 300) {
                log.info(gson.toJson(restApiLog));
            } else if (restApiLog.getStatus() > 400) {
                log.error(gson.toJson(restApiLog));
            } else {
                log.warn(gson.toJson(restApiLog));
            }
        } catch (Exception ex) {
            log.error("Error occured log restApiLog", ex);
        }
    }
}
