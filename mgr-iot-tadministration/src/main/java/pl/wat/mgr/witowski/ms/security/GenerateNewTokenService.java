package pl.wat.mgr.witowski.ms.security;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.wat.mgr.witowski.ms.security.jwt.JwtUtils;
import pl.wat.mgr.witowski.ms.security.secret.SecretDto;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Collections;

@Log4j2
@Service
public class GenerateNewTokenService {
    @Value("${pl.wat.mgr.witowski.ms.mgrauth.url}")
    String mgrauthUrl;
    private RestTemplate restTemplate;

    public GenerateNewTokenService() {
        restTemplate = new RestTemplate();
    }

    public void sendOrderToGenerateNewKey() {
        boolean isSuccesful = false;

        for (int count = 0; count < 10 && !isSuccesful; ++count) {
            try {
                log.info("send for generate new token secret");
                String uri = this.mgrauthUrl + "/security/key";
                ResponseEntity<SecretDto> entity = this.restTemplate.getForEntity(uri, SecretDto.class, Collections.emptyMap());
                SecretDto body = (SecretDto) entity.getBody();
                byte[] publicBytes = Base64.getDecoder().decode(body.getSecret());
                X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                PublicKey pubKey = keyFactory.generatePublic(keySpec);
                JwtUtils.setSecretKey(pubKey);
                isSuccesful = true;
            } catch (Exception var11) {
                log.error("cant connact to serwet ", var11);
                try {
                    log.info("wait 10 sec for next sendOrderToGenerateNewKey");
                    Thread.sleep(10000L);
                } catch (InterruptedException var10) {
                    log.warn("Error occurred wait before next sendOrderToGenerateNewKey", var10);
                }
            }
        }

    }
}
