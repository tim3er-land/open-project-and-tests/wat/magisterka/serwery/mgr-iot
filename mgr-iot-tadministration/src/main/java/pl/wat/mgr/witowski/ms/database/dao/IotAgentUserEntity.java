package pl.wat.mgr.witowski.ms.database.dao;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "iot_agent_user", schema = "public", catalog = "postgres_mgr")
public class IotAgentUserEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "iot_agt_usr_id")
    private long iotAgtUsrId;
    @Basic
    @Column(name = "iot_usr_uid")
    private String iotUsrUid;
    @ManyToOne
    @JoinColumn(name = "iot_agt_id", referencedColumnName = "iot_agt_id", nullable = false)
    private IotAgentEntity iotAgentByIotAgtId;

    public long getIotAgtUsrId() {
        return iotAgtUsrId;
    }

    public void setIotAgtUsrId(long iotAgtUsrId) {
        this.iotAgtUsrId = iotAgtUsrId;
    }

    public String getIotUsrUid() {
        return iotUsrUid;
    }

    public void setIotUsrUid(String iotUsrUid) {
        this.iotUsrUid = iotUsrUid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IotAgentUserEntity that = (IotAgentUserEntity) o;
        return iotAgtUsrId == that.iotAgtUsrId && Objects.equals(iotUsrUid, that.iotUsrUid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iotAgtUsrId, iotUsrUid);
    }

    public IotAgentEntity getIotAgentByIotAgtId() {
        return iotAgentByIotAgtId;
    }

    public void setIotAgentByIotAgtId(IotAgentEntity iotAgentByIotAgtId) {
        this.iotAgentByIotAgtId = iotAgentByIotAgtId;
    }
}
