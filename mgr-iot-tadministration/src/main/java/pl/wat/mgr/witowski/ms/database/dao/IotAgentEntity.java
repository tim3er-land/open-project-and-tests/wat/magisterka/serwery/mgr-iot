package pl.wat.mgr.witowski.ms.database.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "iot_agent")
public class IotAgentEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "iot_agt_id")
    private long iotAgtId;
    @Basic
    @Column(name = "iot_agt_uid")
    private String iotAgtUid;
    @Basic
    @Column(name = "iot_agt_name")
    private String iotAgtName;
    @Basic
    @Column(name = "iot_agt_type_code")
    private String iotAgtTypeCode;
    @Basic
    @Column(name = "iot_agt_config")
    private String iotAgtConfig;
    @Basic
    @Column(name = "iot_agt_pub_key_sign")
    private String iotAgtPubKeySign;
    @Basic
    @Column(name = "iot_agt_insert_by")
    private String iotAgtInsertBy;
    @Basic
    @Column(name = "iot_agt_modify_by")
    private String iotAgtModifyBy;
    @Basic
    @Column(name = "iot_agt_insert_data")
    private LocalDateTime iotAgtInsertData;
    @Basic
    @Column(name = "iot_agt_modify_data")
    private LocalDateTime iotAgtModifyData;
    @Basic
    @Column(name = "iot_agt_act")
    private short iotAgtAct;
    @OneToMany(mappedBy = "iotAgentByIotAgtId")
    private Collection<IotAgentTopicEntity> iotAgentTopicsByIotAgtId;
    @OneToMany(mappedBy = "iotAgentByIotAgtId")
    private Collection<IotAgentUserEntity> iotAgentUsersByIotAgtId;
    @Basic
    @Column(name = "iot_agt_pub_key_decr")
    private String iotAgtPubKeyDecr;

    public long getIotAgtId() {
        return iotAgtId;
    }

    public void setIotAgtId(long iotAgtId) {
        this.iotAgtId = iotAgtId;
    }

    public String getIotAgtUid() {
        return iotAgtUid;
    }

    public void setIotAgtUid(String iotAgtUid) {
        this.iotAgtUid = iotAgtUid;
    }

    public String getIotAgtName() {
        return iotAgtName;
    }

    public void setIotAgtName(String iotAgtName) {
        this.iotAgtName = iotAgtName;
    }

    public String getIotAgtTypeCode() {
        return iotAgtTypeCode;
    }

    public void setIotAgtTypeCode(String iotAgtTypeCode) {
        this.iotAgtTypeCode = iotAgtTypeCode;
    }

    public String getIotAgtConfig() {
        return iotAgtConfig;
    }

    public void setIotAgtConfig(String iotAgtConfig) {
        this.iotAgtConfig = iotAgtConfig;
    }

    public String getIotAgtPubKeySign() {
        return iotAgtPubKeySign;
    }

    public void setIotAgtPubKeySign(String iotAgtPubKeySign) {
        this.iotAgtPubKeySign = iotAgtPubKeySign;
    }

    public String getIotAgtInsertBy() {
        return iotAgtInsertBy;
    }

    public void setIotAgtInsertBy(String iotAgtInsertBy) {
        this.iotAgtInsertBy = iotAgtInsertBy;
    }

    public String getIotAgtModifyBy() {
        return iotAgtModifyBy;
    }

    public void setIotAgtModifyBy(String iotAgtModifyBy) {
        this.iotAgtModifyBy = iotAgtModifyBy;
    }

    public LocalDateTime getIotAgtInsertData() {
        return iotAgtInsertData;
    }


    public void setIotAgtInsertData(LocalDateTime iotAgtInsertData) {
        this.iotAgtInsertData = iotAgtInsertData;
    }

    public LocalDateTime getIotAgtModifyData() {
        return iotAgtModifyData;
    }


    public void setIotAgtModifyData(LocalDateTime iotAgtModifyData) {
        this.iotAgtModifyData = iotAgtModifyData;
    }

    public short getIotAgtAct() {
        return iotAgtAct;
    }

    public void setIotAgtAct(short iotAgtAct) {
        this.iotAgtAct = iotAgtAct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IotAgentEntity that = (IotAgentEntity) o;
        return iotAgtId == that.iotAgtId && iotAgtAct == that.iotAgtAct && Objects.equals(iotAgtUid, that.iotAgtUid) && Objects.equals(iotAgtName, that.iotAgtName) && Objects.equals(iotAgtTypeCode, that.iotAgtTypeCode) && Objects.equals(iotAgtConfig, that.iotAgtConfig) && Objects.equals(iotAgtPubKeySign, that.iotAgtPubKeySign) && Objects.equals(iotAgtInsertBy, that.iotAgtInsertBy) && Objects.equals(iotAgtModifyBy, that.iotAgtModifyBy) && Objects.equals(iotAgtInsertData, that.iotAgtInsertData) && Objects.equals(iotAgtModifyData, that.iotAgtModifyData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iotAgtId, iotAgtUid, iotAgtName, iotAgtTypeCode, iotAgtConfig, iotAgtPubKeySign, iotAgtInsertBy, iotAgtModifyBy, iotAgtInsertData, iotAgtModifyData, iotAgtAct);
    }

    public Collection<IotAgentTopicEntity> getIotAgentTopicsByIotAgtId() {
        return iotAgentTopicsByIotAgtId;
    }

    public void setIotAgentTopicsByIotAgtId(Collection<IotAgentTopicEntity> iotAgentTopicsByIotAgtId) {
        this.iotAgentTopicsByIotAgtId = iotAgentTopicsByIotAgtId;
    }

    public Collection<IotAgentUserEntity> getIotAgentUsersByIotAgtId() {
        return iotAgentUsersByIotAgtId;
    }

    public void setIotAgentUsersByIotAgtId(Collection<IotAgentUserEntity> iotAgentUsersByIotAgtId) {
        this.iotAgentUsersByIotAgtId = iotAgentUsersByIotAgtId;
    }

    public String getIotAgtPubKeyDecr() {
        return iotAgtPubKeyDecr;
    }

    public void setIotAgtPubKeyDecr(String iotAgtPubKeyDecr) {
        this.iotAgtPubKeyDecr = iotAgtPubKeyDecr;
    }
}
