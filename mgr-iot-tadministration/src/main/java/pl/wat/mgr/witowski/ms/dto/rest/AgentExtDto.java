package pl.wat.mgr.witowski.ms.dto.rest;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonDeserialize(builder = AgentExtDto.AgentExtDtoBuilder.class)
public class AgentExtDto extends BasicAgentDto {

    private String agentUid;

    @Builder
    public AgentExtDto(String agentName, String agentTypeCode, String agentUid) {
        super(agentName, agentTypeCode);
        this.agentUid = agentUid;
    }

    @JsonPOJOBuilder
    public static class AgentExtDtoBuilder extends BasicAgentDtoBuilder {

    }
}
