package pl.wat.mgr.witowski.ms.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.wat.mgr.witowski.ms.database.dao.IotAgentRuleEntity;

import java.util.List;

public interface IotAgentRuleRepository extends JpaRepository<IotAgentRuleEntity, String> {
    @Query("select ar from IotAgentRuleEntity ar where ar.iotUsrUid = :userUid and ar.iotAgtRulAct = 1")
    List<IotAgentRuleEntity> getUserRules(@Param("userUid") String userUid);

//    @Query("select ar from IotAgentRuleEntity ar join ar.iotAgentTopicByIotAgtTopicId at where at.iotAgentByIotAgtId = :agent and at.iotAgrTopUid = :topicUid and ar.iotAgtRulUid = :ruleUid and at.iotAgtTopAct = 1 and ar.iotAgtRulAct = 1 ")
//    IotAgentRuleEntity getAgentTopicRule(@Param("agent") IotAgentEntity agent, @Param("topicUid") String topicUid, @Param("ruleUid") String ruleUid);

    @Query("select ar from IotAgentRuleEntity ar where ar.iotUsrUid = :userUid and ar.iotAgtRulAct = 1 and ar.iotAgtRulUid = :ruleUid")
    IotAgentRuleEntity getUserRule(@Param("ruleUid") String ruleUid, @Param("userUid") String userUid);
}
