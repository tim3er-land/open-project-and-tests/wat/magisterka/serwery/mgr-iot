package pl.wat.mgr.witowski.ms.commons.exceptions.handler;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pl.wat.mgr.witowski.ms.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.ms.commons.exceptions.dto.ErrorDto;
import pl.wat.mgr.witowski.ms.commons.exceptions.dto.ErrorReponceDto;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Log4j2
@EnableWebMvc
@ControllerAdvice
public class ApiResponseExceptionHandler extends ResponseEntityExceptionHandler {

    MessageSource messageSource;

    @Autowired
    public ApiResponseExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler({ApiExceptions.class, })
    protected ResponseEntity<ErrorReponceDto> apiExceptionsHandler(Exception ex, WebRequest webRequest) {
        ApiExceptions apiExceptions = (ApiExceptions) ex;
        Locale locale = getLocale(webRequest);
        String message = null;
        try {
            message = messageSource.getMessage(apiExceptions.getDescription(), null, locale);
        } catch (Exception e) {
            log.warn("Can not find message: {} ", apiExceptions.getDescription());
        }
        ErrorReponceDto.ErrorReponceDtoBuilder errorReponceDtoBuilder = ErrorReponceDto.builder()
                .code(apiExceptions.getCode())
                .httpStatus(apiExceptions.getStatus())
                .message(StringUtils.isEmpty(message) ? apiExceptions.getCode() : message);
        if (!CollectionUtils.isEmpty(apiExceptions.getErrorDtos())) {
            errorReponceDtoBuilder.errors(prepareErrors(apiExceptions.getErrorDtos(), locale));
        }
        ErrorReponceDto errorReponceDto = errorReponceDtoBuilder.build();
        return new ResponseEntity<>(errorReponceDto, errorReponceDto.getHttpStatus());
    }

    private List<ErrorDto> prepareErrors(List<ErrorDto> errorDtos, Locale locale) {
        return errorDtos.stream().map(errorDto -> {
            try {
                errorDto.setMessage(messageSource.getMessage(errorDto.getMessage(), null, locale));
            } catch (Exception exception) {
                log.warn("Can not find messaege: {}", errorDto.getMessage());
                errorDto.setMessage(errorDto.getMessage());
            }
            return errorDto;
        }).collect(Collectors.toList());
    }

    private Locale getLocale(WebRequest webRequest) {
        if (((ServletWebRequest) webRequest).getRequest().getHeader("Locale") != null) {
            String locale = ((ServletWebRequest) webRequest).getRequest().getHeader("Locale");
            switch (locale.toUpperCase()) {
                case "PL":
                    return new Locale("pl");
                case "EN":
                    return Locale.ENGLISH;

            }
        }
        return Locale.ENGLISH;
    }
}
