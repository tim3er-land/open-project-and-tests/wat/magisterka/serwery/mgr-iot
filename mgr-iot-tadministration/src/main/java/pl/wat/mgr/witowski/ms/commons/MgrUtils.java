package pl.wat.mgr.witowski.ms.commons;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;

import java.util.Random;

public class MgrUtils {
    private static Random random;
    private static final char[] alphabet = {'a', 'b', 'c', 'd', 'e', 'f', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'q', 'w', 'x', 'y', 'z'};

    public static final Short ACTIVE = Short.valueOf("1");
    public static final Short DIS_ACTIVE = Short.valueOf("0");


    public static String generateNanoId(int size) {
        if (random == null) {
            random = new Random();
        }
        String id = NanoIdUtils.randomNanoId(random, alphabet, size);
        return id;
    }

    public static String generateNanoId() {
        return generateNanoId(10);
    }
}
