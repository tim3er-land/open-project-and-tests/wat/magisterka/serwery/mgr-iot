package pl.wat.mgr.witowski.ms.commons.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.http.HttpStatus;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class RestApiLog {
    private String method;
    private String ms;
    private String remoteAdress;
    private String path;
    private String op;
    private String userUid;
    private Long duraction;
    private Integer status;
    private HttpStatus statusCode;
    private OneWayRestApiLog requestLog;
    private OneWayRestApiLog responceLog;

}
