package pl.wat.mgr.witowski.ms.commons.exceptions.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
@NoArgsConstructor
@JsonDeserialize(builder = ErrorReponceDto.ErrorReponceDtoBuilder.class)
public class ErrorReponceDto extends BasicErrorDto {
    private HttpStatus httpStatus;

    @Builder
    public ErrorReponceDto(String code, String message, List<ErrorDto> errors, HttpStatus httpStatus) {
        super(code, message, errors);
        this.httpStatus = httpStatus;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class ErrorReponceDtoBuilder extends BasicErrorDto.BasicErrorDtoBuilder {

    }
}
