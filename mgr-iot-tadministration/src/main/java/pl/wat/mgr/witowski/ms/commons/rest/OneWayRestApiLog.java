package pl.wat.mgr.witowski.ms.commons.rest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.MultiValueMap;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OneWayRestApiLog {
    private Object body;
    private List<HashMapLog> headers;
    private List<CookieLog> coockie;
    private MultiValueMap<String,String> params;
    private Integer lenght;
}
