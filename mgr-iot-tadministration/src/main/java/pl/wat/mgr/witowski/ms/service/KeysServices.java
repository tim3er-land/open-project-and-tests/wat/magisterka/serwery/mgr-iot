package pl.wat.mgr.witowski.ms.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.wat.mgr.witowski.ms.dto.security.FullKeyDto;
import pl.wat.mgr.witowski.ms.security.secret.MgrSecretDto;
import pl.wat.mgr.witowski.ms.security.secret.SecretDto;

import java.util.Collections;

@Log4j2
@Service
public class KeysServices {

    private final RestTemplate restTemplate;
    private final String mgrAuthUrl;
    private final String mgrIotData;

    @Autowired
    public KeysServices(@Qualifier("dictioranyRestTemplare") RestTemplate restTemplate,
                        @Value("${pl.wat.mgr.witowski.ms.mgrauth.url}") String mgrAuthUrl,
                        @Value("${pl.wat.mgr.witowski.ms.mgriotdata.url}") String mgrIotData) {
        this.restTemplate = restTemplate;
        this.mgrAuthUrl = mgrAuthUrl;
        this.mgrIotData = mgrIotData;
    }

    public FullKeyDto generateAgentNewKeys() {
        FullKeyDto fullKeyDto = restTemplate.getForObject(mgrAuthUrl + "/security/pair", FullKeyDto.class, Collections.EMPTY_MAP);
        return fullKeyDto;
    }

    public MgrSecretDto getMgtIotDataPubKey() {
        MgrSecretDto fullKeyDto = restTemplate.getForObject(mgrIotData + "/security/pub", MgrSecretDto.class, Collections.EMPTY_MAP);
        return fullKeyDto;
    }
}
