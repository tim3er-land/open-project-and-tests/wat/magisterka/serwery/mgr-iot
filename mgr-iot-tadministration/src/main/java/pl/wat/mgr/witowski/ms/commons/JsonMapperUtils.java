package pl.wat.mgr.witowski.ms.commons;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import pl.wat.mgr.witowski.ms.commons.exceptions.ApiExceptions;

import java.util.List;
import java.util.Map;

@Log4j2
public class JsonMapperUtils {
    private static ObjectMapper objectMapper;

    public static String convertMapToString(Map<String, Object> map) {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }
        try {
            return objectMapper.writeValueAsString(map);
        } catch (Exception ex) {
            log.error("Can not convert: {} to json ", map);
            return null;
        }
    }

    public static ApiExceptions convertStringToApiException(String body) {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }
        try {
            return objectMapper.readValue(body, ApiExceptions.class);
        } catch (Exception ex) {
            log.error("Can not convert: {} to json ", body);
            return null;
        }
    }

    public static <T> List<T> convertJsonToList(String body, Class<T> returnClass) {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        }
        try {
            return objectMapper.readValue(body, objectMapper.getTypeFactory().constructCollectionType(List.class, returnClass));
        } catch (Exception ex) {
            log.error("Can not convert: {} to json ", body);
            return null;
        }
    }
}
