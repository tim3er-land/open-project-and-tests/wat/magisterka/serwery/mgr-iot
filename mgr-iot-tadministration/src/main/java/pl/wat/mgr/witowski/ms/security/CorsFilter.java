/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wat.mgr.witowski.ms.security;

import lombok.Builder;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CorsFilter implements Filter {

    private static final String HTTP_HEADER_ORIGIN = "Origin";


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        res.setHeader("Access-Control-Allow-Origin", req.getHeader("Origin"));
        res.setHeader("Access-Control-Allow-Credentials", "true");
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        res.setHeader("Access-Control-Max-Age", "3600");
        res.setHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With, remember-me");

        chain.doFilter(req, res);
    }

    @Override
    public void destroy() {
    }

    private String getAllowedOrigin(String requestOrigin) {
        Pattern pattern;
        Matcher matcher;
        String allowedOrigin = "*";
        if(StringUtils.isEmpty(requestOrigin)) {
            return allowedOrigin;
        }

        List<String> allowedOrigins = Arrays.asList("*");
        for(String origin : allowedOrigins) {
            if(origin.trim().equalsIgnoreCase("*")) {
                continue;
            } else {
                origin = origin.replaceAll("\\*", ".*?");
                pattern = Pattern.compile(origin);
                matcher = pattern.matcher(requestOrigin);
                if(matcher.matches()) {
                    return requestOrigin;
                }
            }
        }
        return allowedOrigin;
    }

}
