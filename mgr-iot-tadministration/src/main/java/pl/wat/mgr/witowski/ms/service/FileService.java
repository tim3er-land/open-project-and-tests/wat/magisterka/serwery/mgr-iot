package pl.wat.mgr.witowski.ms.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import pl.wat.mgr.witowski.ms.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.ms.dto.security.FullKeyDto;
import pl.wat.mgr.witowski.ms.security.secret.MgrSecretDto;
import pl.wat.mgr.witowski.ms.security.secret.SecretDto;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Log4j2
@Service
public class FileService {

    @Value("${pl.wat.mgr.witowski.ms.mgriottadministration.zippath}")
    private String zippath;

    public File prepateZipFile(String fileUid, FullKeyDto signFullKeyDto, FullKeyDto decryptFullKeyDto, MgrSecretDto servicesMgtIotDataPubKey) {
        try {
            String pathname = zippath + "/" + fileUid + ".zip";
            FileOutputStream fos = new FileOutputStream(pathname);
            ZipOutputStream zipOS = new ZipOutputStream(fos);
            writeToZipFile(ResourceUtils.getFile("classpath:zip/agent-example.rar"), zipOS);
            writeToZipFile(ResourceUtils.getFile("classpath:zip/mgr-iot-core-1.0-SNAPSHOT.jar"), zipOS);
            writeToZipFile(ResourceUtils.getFile("classpath:zip/mgr-iot-core-1.0-SNAPSHOT-jar-with-dependencies.jar"), zipOS);
            writeToZipFile(ResourceUtils.getFile("classpath:zip/jade.jar"), zipOS);
            writeToZipFile(signFullKeyDto.getPriKey(), "sign/private.key", zipOS);
            writeToZipFile(signFullKeyDto.getPubKey(), "sign/public.key", zipOS);
            writeToZipFile(decryptFullKeyDto.getPriKey(), "encrypt/private.key", zipOS);
            writeToZipFile(decryptFullKeyDto.getPubKey(), "encrypt/public.key", zipOS);
            writeToZipFile(servicesMgtIotDataPubKey.getSecretSign(), "secrets/secretSigh.key", zipOS);
            writeToZipFile(servicesMgtIotDataPubKey.getSecretDecrypt(), "secrets/secretDecrypt.key", zipOS);

            zipOS.close();
            fos.close();
            File file = new File(pathname);
            return file;
        } catch (Exception ex) {
            log.error("Error occured while prepare zipFile with uid: {}", fileUid, ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-002-003", "mgr-iot-administration-002-003");
        }
    }

    private void writeToZipFile(String key, String path, ZipOutputStream zipStream) throws IOException {
        InputStream stream = new ByteArrayInputStream(key.getBytes(StandardCharsets.UTF_8));
        ZipEntry zipEntry = new ZipEntry(path);
        zipStream.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = stream.read(bytes)) >= 0) {
            zipStream.write(bytes, 0, length);
        }

        zipStream.closeEntry();
        stream.close();
    }

    private void writeToZipFile(File file, ZipOutputStream zipStream)
            throws IOException {


        FileInputStream fis = new FileInputStream(file);
        ZipEntry zipEntry = new ZipEntry(file.getName());
        zipStream.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zipStream.write(bytes, 0, length);
        }

        zipStream.closeEntry();
        fis.close();
    }
}
