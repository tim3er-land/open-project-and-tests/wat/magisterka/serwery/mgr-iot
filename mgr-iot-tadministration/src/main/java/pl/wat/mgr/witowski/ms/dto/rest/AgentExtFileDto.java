package pl.wat.mgr.witowski.ms.dto.rest;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonDeserialize(builder = AgentExtFileDto.AgentExtFileDtoBuilder.class)
public class AgentExtFileDto extends AgentExtDto {

    private byte[] file;

    @Builder
    public AgentExtFileDto(String agentName, String agentTypeCode, String agentUid, byte[] file) {
        super(agentName, agentTypeCode, agentUid);
        this.file = file;
    }

    @JsonPOJOBuilder
    public static class AgentExtFileDtoBuilder extends AgentExtDtoBuilder {

    }
}
