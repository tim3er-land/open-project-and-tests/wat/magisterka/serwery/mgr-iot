package pl.wat.mgr.witowski.ms.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import pl.wat.mgr.witowski.ms.commons.exceptions.dto.ErrorReponceDto;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Autowired
    MessageSource messageSource;
    @Autowired
    ObjectMapper objectMapper;

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        Locale locale = getLocale(httpServletRequest);
        ErrorReponceDto.ErrorReponceDtoBuilder errorReponceDtoBuilder = ErrorReponceDto.builder()
                .code("auth-000-000-401")
                .httpStatus(HttpStatus.UNAUTHORIZED)
                .message(messageSource.getMessage("auth-000-000-401", null, locale));
        ErrorReponceDto errorReponceDto = errorReponceDtoBuilder.build();
        try {
            httpServletResponse.sendError(HttpStatus.UNAUTHORIZED.value(), objectMapper.writeValueAsString(errorReponceDto));
        } catch (Exception ex) {
            httpServletResponse.sendError(HttpStatus.UNAUTHORIZED.value());
        }
    }

    private Locale getLocale(HttpServletRequest webRequest) {
        if (webRequest.getHeader("Locale") != null) {
            String locale = webRequest.getHeader("Locale");
            switch (locale.toUpperCase()) {
                case "PL":
                    return new Locale("pl");
                case "EN":
                    return Locale.ENGLISH;

            }
        }
        return Locale.ENGLISH;
    }
}
