package pl.wat.mgr.witowski.ms.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.wat.mgr.witowski.ms.database.dao.IotAgentEntity;
import pl.wat.mgr.witowski.ms.database.dao.IotAgentTopicEntity;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

public interface IotAgentTopicRepository extends JpaRepository<IotAgentTopicEntity, Long> {

    @Query("select at from IotAgentTopicEntity at where at.iotAgrTopUid = :topicUid and at.iotAgentByIotAgtId = :agent and at.iotAgtTopAct = 1 ")
    IotAgentTopicEntity findTopicByUid(@Param("topicUid") String topicUid, @Param("agent") IotAgentEntity agent);

    @Query("select at from IotAgentTopicEntity at where at.iotAgentByIotAgtId = :agent and at.iotAgtTopAct = 1 ")
    List<IotAgentTopicEntity> findTopicsByUid(@Param("agent") IotAgentEntity agent);

    @Modifying
    @Transactional
    @Query("update IotAgentTopicEntity set iotAgtTopAct = 0, iotAgtTopModifyBy = :modifyUid, iotAgtTopModifyDate = :modifyDate where iotAgtTopAct = 1 and iotAgentByIotAgtId = :agent")
    void diableOldTopic(@Param("agent") IotAgentEntity agent, @Param("modifyDate") LocalDateTime modifyDate, @Param("modifyUid") String modifyUid);
}
