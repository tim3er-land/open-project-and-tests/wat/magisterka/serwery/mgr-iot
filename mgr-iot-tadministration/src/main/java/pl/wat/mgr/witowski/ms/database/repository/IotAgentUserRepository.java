package pl.wat.mgr.witowski.ms.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.wat.mgr.witowski.ms.database.dao.IotAgentUserEntity;

public interface IotAgentUserRepository extends JpaRepository<IotAgentUserEntity, Long> {

}
