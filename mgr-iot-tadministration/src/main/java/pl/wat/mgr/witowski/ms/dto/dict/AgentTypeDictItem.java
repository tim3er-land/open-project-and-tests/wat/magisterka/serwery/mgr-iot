package pl.wat.mgr.witowski.ms.dto.dict;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentTypeDictItem {
    private List<AgentTypeTopicDict> agentTypeRuleDictList;
}
