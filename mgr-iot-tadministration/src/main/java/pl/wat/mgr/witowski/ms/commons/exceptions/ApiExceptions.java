package pl.wat.mgr.witowski.ms.commons.exceptions;

import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import pl.wat.mgr.witowski.ms.commons.exceptions.dto.ErrorDto;

import java.util.List;

@NoArgsConstructor
public class ApiExceptions extends RuntimeException {


    private HttpStatus status;
    private String code;
    private String description;
    private String subCode;
    private List<ErrorDto> errorDtos;

    public ApiExceptions(HttpStatus status, String code, String description) {
        this.status = status;
        this.code = code;
        this.description = description;
    }

    public ApiExceptions(HttpStatus status, String code, String description, String subCode) {
        this.status = status;
        this.code = code;
        this.description = description;
        this.subCode = subCode;
    }

    public ApiExceptions(HttpStatus status, String code, String description, List<ErrorDto> errorDtos) {
        this.status = status;
        this.code = code;
        this.description = description;
        this.errorDtos = errorDtos;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public List<ErrorDto> getErrorDtos() {
        return errorDtos;
    }

    public void setErrorDtos(List<ErrorDto> errorDtos) {
        this.errorDtos = errorDtos;
    }
}
