package pl.wat.mgr.witowski.ms.commons.exceptions.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author: Piotr Witowski
 * @date: 27.09.2021
 * @project tim3erland-iot
 * Day of week: poniedziałek
 */

@Data
@NoArgsConstructor
@JsonDeserialize(builder = BasicErrorDto.BasicErrorDtoBuilder.class)
public class BasicErrorDto {
    private String code;
    private String message;
    private List<ErrorDto> errors;

    @Builder
    public BasicErrorDto(String code, String message, List<ErrorDto> errors) {
        this.code = code;
        this.message = message;
        this.errors = errors;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class BasicErrorDtoBuilder {

    }

}
