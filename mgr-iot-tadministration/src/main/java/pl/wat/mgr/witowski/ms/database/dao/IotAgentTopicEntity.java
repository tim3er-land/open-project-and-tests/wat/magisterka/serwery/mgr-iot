package pl.wat.mgr.witowski.ms.database.dao;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "iot_agent_topic", schema = "public", catalog = "postgres_mgr")
public class IotAgentTopicEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "iot_agr_top_id")
    private long iotAgrTopId;
    @Basic
    @Column(name = "iot_agr_top_uid")
    private String iotAgrTopUid;
    @Basic
    @Column(name = "iot_agt_top_act")
    private short iotAgtTopAct;
    @Basic
    @Column(name = "iot_agt_top_insert_by")
    private String iotAgtTopInsertBy;
    @Basic
    @Column(name = "iot_agt_top_name")
    private String iotAgtTopName;
    @Basic
    @Column(name = "iot_agt_top_modify_by")
    private String iotAgtTopModifyBy;
    @Basic
    @Column(name = "iot_agt_top_insert_date")
    private LocalDateTime iotAgtTopInsertDate;
    @Basic
    @Column(name = "iot_agt_top_modify_date")
    private LocalDateTime iotAgtTopModifyDate;
    @ManyToOne
    @JoinColumn(name = "iot_agt_id", referencedColumnName = "iot_agt_id", nullable = false)
    private IotAgentEntity iotAgentByIotAgtId;

    public long getIotAgrTopId() {
        return iotAgrTopId;
    }

    public void setIotAgrTopId(long iotAgrTopId) {
        this.iotAgrTopId = iotAgrTopId;
    }

    public String getIotAgrTopUid() {
        return iotAgrTopUid;
    }

    public void setIotAgrTopUid(String iotAgrTopUid) {
        this.iotAgrTopUid = iotAgrTopUid;
    }

    public short getIotAgtTopAct() {
        return iotAgtTopAct;
    }

    public void setIotAgtTopAct(short iotAgtTopAct) {
        this.iotAgtTopAct = iotAgtTopAct;
    }

    public String getIotAgtTopInsertBy() {
        return iotAgtTopInsertBy;
    }

    public void setIotAgtTopInsertBy(String iotAgtTopInsertBy) {
        this.iotAgtTopInsertBy = iotAgtTopInsertBy;
    }

    public String getIotAgtTopName() {
        return iotAgtTopName;
    }

    public void setIotAgtTopName(String iotAgtTopName) {
        this.iotAgtTopName = iotAgtTopName;
    }

    public String getIotAgtTopModifyBy() {
        return iotAgtTopModifyBy;
    }

    public void setIotAgtTopModifyBy(String iotAgtTopModifyBy) {
        this.iotAgtTopModifyBy = iotAgtTopModifyBy;
    }

    public LocalDateTime getIotAgtTopInsertDate() {
        return iotAgtTopInsertDate;
    }

    public void setIotAgtTopInsertDate(LocalDateTime iotAgtTopInsertDate) {
        this.iotAgtTopInsertDate = iotAgtTopInsertDate;
    }

    public LocalDateTime getIotAgtTopModifyDate() {
        return iotAgtTopModifyDate;
    }

    public void setIotAgtTopModifyDate(LocalDateTime iotAgtTopModifyDate) {
        this.iotAgtTopModifyDate = iotAgtTopModifyDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IotAgentTopicEntity that = (IotAgentTopicEntity) o;
        return iotAgrTopId == that.iotAgrTopId && iotAgtTopAct == that.iotAgtTopAct && Objects.equals(iotAgrTopUid, that.iotAgrTopUid) && Objects.equals(iotAgtTopInsertBy, that.iotAgtTopInsertBy) && Objects.equals(iotAgtTopName, that.iotAgtTopName) && Objects.equals(iotAgtTopModifyBy, that.iotAgtTopModifyBy) && Objects.equals(iotAgtTopInsertDate, that.iotAgtTopInsertDate) && Objects.equals(iotAgtTopModifyDate, that.iotAgtTopModifyDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iotAgrTopId, iotAgrTopUid, iotAgtTopAct, iotAgtTopInsertBy, iotAgtTopName, iotAgtTopModifyBy, iotAgtTopInsertDate, iotAgtTopModifyDate);
    }

    public IotAgentEntity getIotAgentByIotAgtId() {
        return iotAgentByIotAgtId;
    }

    public void setIotAgentByIotAgtId(IotAgentEntity iotAgentByIotAgtId) {
        this.iotAgentByIotAgtId = iotAgentByIotAgtId;
    }

    public IotAgentTopicEntity() {
    }

    public IotAgentTopicEntity(String iotAgrTopUid, String iotAgtTopName) {
        this.iotAgrTopUid = iotAgrTopUid;
        this.iotAgtTopName = iotAgtTopName;
        this.iotAgtTopAct = 1;
    }
}
