package pl.wat.mgr.witowski.ms.contoller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import pl.wat.mgr.witowski.ms.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.ms.dto.*;
import pl.wat.mgr.witowski.ms.dto.rest.AgentExtDto;
import pl.wat.mgr.witowski.ms.dto.rest.BasicAgentDto;
import pl.wat.mgr.witowski.ms.service.AgentService;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

@Log4j2
@CrossOrigin
@RestController
public class AgentContoller {

    private final AgentService agentService;

    @Autowired
    public AgentContoller(AgentService agentService) {
        this.agentService = agentService;
    }

    @PreAuthorize("hasRole('USER')")
    @PutMapping(path = "/iot/agent", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AgentExtDto> addNewAgent(@RequestBody BasicAgentDto basicAgentDto) {
        AgentExtDto agentExtDto = agentService.addNewAgent(basicAgentDto);
        return new ResponseEntity<>(agentExtDto, HttpStatus.CREATED);
    }

    @GetMapping(path = "/iot/agent/{agentUid}/zip")
    public ResponseEntity<Resource> getAgentZip(@PathVariable("agentUid") String agentUid) {
        File file = agentService.getAgentZip(agentUid);
        try {
            HttpHeaders header = new HttpHeaders();
            header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + agentUid + ".zip");
            header.add("Cache-Control", "no-cache, no-store, must-revalidate");
            header.add("Pragma", "no-cache");
            header.add("Expires", "0");
            InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
            ResponseEntity<Resource> responseEntity = ResponseEntity.ok()
                    .headers(header)
                    .contentLength(file.length())
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
            file.deleteOnExit();
            return responseEntity;
        } catch (Exception ex) {
            log.error("Error occured while method getAgentZip for agent: {}", agentUid, ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-002-001", "mgr-iot-administration-002-001");
        }
    }

    @GetMapping(path = "/iot/agent/{agentUid}/zip/v2")
    public ResponseEntity<Resource> getAgentZipV2(@PathVariable("agentUid") String agentUid) {
        File file = agentService.getAgentZip(agentUid);
        try {
            HttpHeaders header = new HttpHeaders();
            header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + agentUid + ".zip");
            header.add("Cache-Control", "no-cache, no-store, must-revalidate");
            header.add("Pragma", "no-cache");
            header.add("Expires", "0");
            InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
            ResponseEntity<Resource> responseEntity = ResponseEntity.ok()
                    .headers(header)
                    .contentLength(file.length())
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
            file.deleteOnExit();
            return responseEntity;
        } catch (Exception ex) {
            log.error("Error occured while method getAgentZip for agent: {}", agentUid, ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-002-001", "mgr-iot-administration-002-001");
        }
    }

    @GetMapping(path = "/iot/agent")//002
    public ResponseEntity<List<AgentExtDto>> getUserIotAgents() {
        List<AgentExtDto> agentExtDto = agentService.getUserIotAgents();
        if (CollectionUtils.isEmpty(agentExtDto)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(agentExtDto);
    }

    @DeleteMapping(path = "/iot/agent/{agentUid}")//003
    public ResponseEntity<Void> deleteUserAgent(@PathVariable("agentUid") String agentUid) {
        agentService.deleteUserAgent(agentUid);
        return ResponseEntity.noContent().build();
    }

//    @PostMapping(path = "/iot/agent/{agentUid}")//003
//    public ResponseEntity<Void> deleteUserAgent(@PathVariable("agentUid") String agentUid) {
//        agentService.deleteUserAgent(agentUid);
//        return ResponseEntity.noContent().build();
//    }


    @GetMapping(path = "/iot/agent/{agentUid}/topic")//004
    public ResponseEntity<List<AgentTopic>> getAgentTopics(@PathVariable("agentUid") String agentUid) {
        List<AgentTopic> agentTopics = agentService.getAgentTopics(agentUid);
        if (CollectionUtils.isEmpty(agentTopics)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(agentTopics);
    }

    @PreAuthorize("hasRole('INNER_USER')")
    @PutMapping(path = "/iot/agent/{agentUid}/topic")//008
    public ResponseEntity<Void> addAgentTopic(@PathVariable("agentUid") String agentUid, @RequestParam("topic") List<String> topic) {
        agentService.addAgentTopic(agentUid, topic);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @PreAuthorize("hasRole('INNER_USER')")
    @DeleteMapping(path = "/iot/agent/{agentUid}/topic")//010
    public ResponseEntity<Void> deleteAgentTopics(@PathVariable("agentUid") String agentUid) {
        agentService.deleteAgentTopic(agentUid);
        return ResponseEntity.noContent().build();
    }


    @GetMapping(path = "/iot/rule")//005
    public ResponseEntity<List<AgentRule>> getUserRule() {
        List<AgentRule> agentTopics = agentService.getAgentTopicRules();
        if (CollectionUtils.isEmpty(agentTopics)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(agentTopics);
    }

    @PostMapping(path = "/iot/rule/{ruleUid}")//006
    public ResponseEntity<Void> modifyAgentRule(@PathVariable("ruleUid") String ruleUid, @RequestBody AgentRuleBasic agentRuleBasic) {
        agentService.modifyAgentRule(ruleUid, agentRuleBasic);
        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/iot/rule")//007
    public ResponseEntity<Void> addUserRule(@RequestBody AgentRuleBasic agentRuleBasic) {
        agentService.addAgentRule(agentRuleBasic);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(path = "/iot/rule/{ruleUid}")
    public ResponseEntity<Void> deleteUserRule(@PathVariable("ruleUid") String ruleUid) {
        agentService.deleteAgentRule(ruleUid);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(path = "/iot/agent/{agentUid}")
    public ResponseEntity<AgentInfoDto> getAgentInfo(@PathVariable("agentUid") String agentUid) {
        AgentInfoDto agentInfo = agentService.getAgentInfo(agentUid);
        return ResponseEntity.ok(agentInfo);
    }


}
