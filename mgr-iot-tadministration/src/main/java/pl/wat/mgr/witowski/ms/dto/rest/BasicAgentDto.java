package pl.wat.mgr.witowski.ms.dto.rest;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonDeserialize(
        builder = BasicAgentDto.BasicAgentDtoBuilder.class
)
public class BasicAgentDto {
    private String agentName;
    private String agentTypeCode;

    @Builder
    public BasicAgentDto(String agentName, String agentTypeCode) {
        this.agentName = agentName;
        this.agentTypeCode = agentTypeCode;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class BasicAgentDtoBuilder {

    }
}
