package pl.wat.mgr.witowski.ms.service.validation;

import org.springframework.util.StringUtils;
import pl.wat.mgr.witowski.ms.commons.BasicValidationServices;
import pl.wat.mgr.witowski.ms.database.dao.IotAgentRuleEntity;
import pl.wat.mgr.witowski.ms.dto.AgentRuleBasic;
import pl.wat.mgr.witowski.ms.dto.dict.DictionaryItemDto;

import java.util.List;

public class EdithAgentRuleValidation extends BasicValidationServices {
    private IotAgentRuleEntity iotAgentRuleEntity;
    private List<DictionaryItemDto> dictionaryItemDtos;
    private AgentRuleBasic agentRuleBasic;

    public EdithAgentRuleValidation(String errorCode, String errorDescription, IotAgentRuleEntity iotAgentRuleEntity, List<DictionaryItemDto> dictionaryItemDtos, AgentRuleBasic agentRuleBasic) {
        super(errorCode, errorDescription);
        this.iotAgentRuleEntity = iotAgentRuleEntity;
        this.dictionaryItemDtos = dictionaryItemDtos;
        this.agentRuleBasic = agentRuleBasic;
    }

    @Override
    public void validate() {
        if (iotAgentRuleEntity == null) {
            addNewErrorDto("ruleUid", "mgr-iot-administration-val-005-001", "mgr-iot-administration-val-005-001");
        }

        if (!StringUtils.hasText(agentRuleBasic.getRuleCode())) {
            addNewErrorDto("ruleCode", "mgr-iot-administration-val-005-002", "mgr-iot-administration-val-005-002");
        }

        if (!dictionaryItemDtos.stream()
                .filter(dictionaryItemDto -> dictionaryItemDto.getItemCode().equalsIgnoreCase(agentRuleBasic.getRuleCode()))
                .findFirst().isPresent()) {
            addNewErrorDto("ruleCode", "mgr-iot-administration-val-005-003", "mgr-iot-administration-val-005-003");
        }

//        if (!StringUtils.hasText(agentRuleBasic.getRuleConfig())) {
//            addNewErrorDto("ruleConfig", "mgr-iot-administration-val-005-004", "mgr-iot-administration-val-005-004");
//        }

        super.validate();

    }
}
