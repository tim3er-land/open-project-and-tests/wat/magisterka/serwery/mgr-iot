package pl.wat.mgr.witowski.ms.security.jwt;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class SecurityUtils {
    public static String hashString(String input) {
        return Hashing.sha512()
                .hashString(input, StandardCharsets.UTF_8)
                .toString();
    }
}
