package pl.wat.mgr.witowski.ms.service.validation;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.PatternMatchUtils;
import org.springframework.util.StringUtils;
import pl.wat.mgr.witowski.ms.commons.BasicValidationServices;
import pl.wat.mgr.witowski.ms.dto.AgentRuleBasic;
import pl.wat.mgr.witowski.ms.dto.dict.AgentRuleDict;
import pl.wat.mgr.witowski.ms.dto.dict.DictionaryItemDto;

import java.util.List;
import java.util.Optional;

public class AddEdithAgentRuleValidation extends BasicValidationServices {
    private List<DictionaryItemDto> dictionaryItemDtos;
    private ObjectMapper objectMapper;
    private AgentRuleBasic agentRuleBasic;
    private boolean edith;

    public AddEdithAgentRuleValidation(String errorCode, String errorDescription, List<DictionaryItemDto> dictionaryItemDtos, AgentRuleBasic agentRuleBasic, boolean edith) {
        super(errorCode, errorDescription);
        this.dictionaryItemDtos = dictionaryItemDtos;
        this.agentRuleBasic = agentRuleBasic;
        this.edith = edith;
        objectMapper = new ObjectMapper();
    }

    @Override
    public void validate() {
        if (!StringUtils.hasText(agentRuleBasic.getRuleCode())) {
            addNewErrorDto("ruleCode", "mgr-iot-administration-val-006-002", "mgr-iot-administration-val-006-002");
        }

        boolean rulePresent = dictionaryItemDtos.stream()
                .filter(dictionaryItemDto -> dictionaryItemDto.getItemCode().equalsIgnoreCase(agentRuleBasic.getRuleCode()))
                .findFirst().isPresent();
        if (!rulePresent) {
            addNewErrorDto("ruleCode", "mgr-iot-administration-val-006-003", "mgr-iot-administration-val-006-003");
        }

        if (!agentRuleBasic.getRuleConfig().containsKey("config") || (agentRuleBasic.getRuleConfig().containsKey("config") && !StringUtils.hasText(String.valueOf(agentRuleBasic.getRuleConfig().get("config"))))) {
            addNewErrorDto("ruleConfig", "mgr-iot-administration-val-006-004", "mgr-iot-administration-val-006-004");
        }

        if (rulePresent) {
            Optional<DictionaryItemDto> itemDtoOptional = dictionaryItemDtos.stream()
                    .filter(dictionaryItemDto -> dictionaryItemDto.getItemCode().equalsIgnoreCase(agentRuleBasic.getRuleCode()))
                    .findFirst();
            AgentRuleDict agentRuleDict = objectMapper.convertValue(itemDtoOptional.get().getItemJson(), AgentRuleDict.class);
            String ruleConfigApper = String.valueOf(agentRuleBasic.getRuleConfig().get("config")).toUpperCase();
            if (!validateSelect(agentRuleDict, ruleConfigApper) || !validateFrom(agentRuleDict, ruleConfigApper)
                    || !validateWhere(agentRuleDict, ruleConfigApper) || !validateInto(agentRuleDict, ruleConfigApper)) {
                addNewErrorDto("ruleConfig", "mgr-iot-administration-val-006-005", "mgr-iot-administration-val-006-005");
            }
        }
        super.validate();

    }

    private boolean validateInto(AgentRuleDict agentRuleDict, String ruleConfig) {
        if (ruleConfig.contains("INTO") && !agentRuleDict.getIsWhere())
            return false;
        if (ruleConfig.contains("INTO")) {
            int whereIndex = ruleConfig.indexOf("WHERE");
            int intoIndex = ruleConfig.indexOf("INTO");
            if (intoIndex < whereIndex) {
                return false;
            }
        }
        return true;
    }

    private boolean validateWhere(AgentRuleDict agentRuleDict, String ruleConfig) {
        if (ruleConfig.contains("WHERE") && !agentRuleDict.getIsWhere())
            return false;
        if (ruleConfig.contains("WHERE")) {
            int whereIndex = ruleConfig.indexOf("WHERE");
            int fromIndex = ruleConfig.indexOf("FROM");
            if (fromIndex > whereIndex) {
                return false;
            }
        }
        return true;
    }

    private boolean validateFrom(AgentRuleDict agentRuleDict, String ruleConfig) {
        if (agentRuleDict.getIsFrom() && !ruleConfig.contains("FROM"))
            return false;
        int fromIndex = ruleConfig.indexOf("FROM");
        int selectIndex = ruleConfig.indexOf("SELECT");
        if (fromIndex < selectIndex) {
            return false;
        }
        return true;
    }

    private boolean validateSelect(AgentRuleDict agentRuleDict, String ruleConfig) {
        if (agentRuleDict.getIsSelect() && !ruleConfig.contains("SELECT"))
            return false;
        return true;
    }
}
