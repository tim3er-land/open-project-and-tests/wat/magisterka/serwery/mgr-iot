package pl.wat.mgr.witowski.ms.dto.dict;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentRuleDict {
    private Boolean isSelect;
    private Boolean isFrom;
    private Boolean isWhere;
    private Boolean isInto;
}
