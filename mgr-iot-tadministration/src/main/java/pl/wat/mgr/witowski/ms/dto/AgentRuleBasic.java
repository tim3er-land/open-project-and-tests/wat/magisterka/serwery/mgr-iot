package pl.wat.mgr.witowski.ms.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentRuleBasic {
    private String ruleCode;
    private HashMap<String,Object> ruleConfig;
}
