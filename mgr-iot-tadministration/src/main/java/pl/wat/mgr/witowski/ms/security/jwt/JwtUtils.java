package pl.wat.mgr.witowski.ms.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.wat.mgr.witowski.ms.commons.exceptions.ApiExceptions;

import javax.servlet.http.HttpServletRequest;
import java.security.PublicKey;
import java.util.Date;
import java.util.List;

@Log4j2
public class JwtUtils {
    public static final String KEY_UUID = "UUID";
    public static final String KEY_ROLE = "ROLE";
    public static final String KEY_TUID = "TUID";
    public static final String KEY_DATE = "DATE";
    public static final String KEY_LOGIN = "LOGIN";
    private static PublicKey secretKey = null;

    public static PublicKey getSecretKey() {
        return secretKey;
    }

    public static void setSecretKey(PublicKey secretKey) {
        JwtUtils.secretKey = secretKey;
    }

    public static String getUsername(String token) {
        return (String) Jwts.parser().setSigningKey(getSecretKey()).parseClaimsJws(token).getBody().get(KEY_LOGIN);
    }

    public static String getUsername() {
        return getUsername(extractToken());
    }

    public static String extractToken() {
        if (SecurityContextHolder.getContext() == null || SecurityContextHolder.getContext().getAuthentication() == null)
            return KeyServiceUtils.getMicroToken();
        return String.valueOf(SecurityContextHolder.getContext().getAuthentication().getCredentials());
    }

    public static List<String> getRole() {
        return getRole(extractToken());
    }

    public static List<String> getRole(String token) {
        return (List<String>) Jwts.parser().setSigningKey(getSecretKey()).parseClaimsJws(token).getBody().get(KEY_ROLE);
    }

    public static String getUUID(String token) {
        try {
            return (String) Jwts.parser().setSigningKey(getSecretKey()).parseClaimsJws(token).getBody().get(KEY_UUID);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getUUID() {
        return getUUID(extractToken());
    }

    public static String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public static boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(getSecretKey()).parse(token);
            return true;
        } catch (Exception e) {
            log.warn("can not parser JWT token", e);
            throw new ApiExceptions(HttpStatus.UNAUTHORIZED, "auth-000-000-401", "auth-000-000-401");
        }
    }

    public static boolean validateToken() {
        return validateToken(extractToken());
    }

    public static Date getExpireTokenDate() {
        return JwtUtils.getExpireTokenDate(extractToken());
    }

    private static Date getExpireTokenDate(String extractToken) {
        Claims claims = Jwts.parser()
                .setSigningKey(getSecretKey())
                .parseClaimsJws(extractToken)
                .getBody();
        return new Date(Long.valueOf(claims.get(JwtUtils.KEY_DATE).toString()));
    }

    public static String getTokenUUID(String token) {
        return (String) Jwts.parser().setSigningKey(getSecretKey()).parseClaimsJws(token).getBody().get(KEY_TUID);
    }

    public static String getTokenUUID() {
        return getTokenUUID(extractToken());
    }
}
