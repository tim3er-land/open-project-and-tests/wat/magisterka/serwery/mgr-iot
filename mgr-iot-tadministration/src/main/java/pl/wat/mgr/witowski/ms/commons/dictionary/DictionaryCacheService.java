package pl.wat.mgr.witowski.ms.commons.dictionary;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.wat.mgr.witowski.ms.commons.JsonMapperUtils;
import pl.wat.mgr.witowski.ms.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.ms.dto.dict.DictionaryItemDto;

import java.util.HashMap;
import java.util.List;

@Log4j2
@Service
public class DictionaryCacheService {

    private final ObjectMapper objectMapper;
    @Value("${pl.wat.mgr.witowski.ms.mgrdict.url}")
    private String mgrdictUrl;

    @Autowired
    @Qualifier("dictioranyRestTemplare")
    private RestTemplate restTemplate;

    public DictionaryCacheService() {
        this.objectMapper = new ObjectMapper();
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Cacheable("dictionary_cache")
    public List<DictionaryItemDto> getDictionaryItems(String dictionaryCode) {
        final String dictionaryUrl = mgrdictUrl + "/dict/{code}/list";
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(dictionaryUrl, String.class, dictionaryCode);
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            if (responseEntity.getStatusCode() == HttpStatus.NO_CONTENT) {
                throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "all-dict-000-001", "all-dict-000-001");
            }
            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                return JsonMapperUtils.convertJsonToList(responseEntity.getBody(), DictionaryItemDto.class);
            }
        } else if (responseEntity.getStatusCode().is5xxServerError()) {
            ApiExceptions apiExceptions = JsonMapperUtils.convertStringToApiException(responseEntity.getBody());
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "all-dict-000-002", "all-dict-000-001", apiExceptions != null ? apiExceptions.getCode() : "");
        }
        ApiExceptions apiExceptions = JsonMapperUtils.convertStringToApiException(responseEntity.getBody());
        throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "all-dict-000-003", "all-dict-000-001", apiExceptions != null ? apiExceptions.getCode() : "");
    }

    @Cacheable("dictionary_cache_item")
    public DictionaryItemDto getDictionaryItemByCode(String dictionaryCode, String ItemCode) {
        List<DictionaryItemDto> dictionaryItems = getDictionaryItems(dictionaryCode);
        return dictionaryItems.stream()
                .filter(dictionaryItemDto -> dictionaryItemDto.getItemCode().equalsIgnoreCase(ItemCode))
                .findFirst()
                .orElseThrow(() -> new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "all-dict-000-000", "all-dict-000-000"));
    }

    public <T> T convertJsonToList(HashMap<String, Object> body , Class<T> returnClass) {
        try {
            return objectMapper.convertValue(body, returnClass);
        } catch (Exception ex) {
            log.error("Can not convert: {} to json ", body, ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "all-dict-000-005", "all-dict-000-005");
        }
    }
}
