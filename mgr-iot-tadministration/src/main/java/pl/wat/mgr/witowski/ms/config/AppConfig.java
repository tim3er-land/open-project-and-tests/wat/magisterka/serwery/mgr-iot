package pl.wat.mgr.witowski.ms.config;

import com.google.common.cache.CacheBuilder;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.CacheManager;
import org.springframework.cache.guava.GuavaCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;
import pl.wat.mgr.witowski.ms.security.GenerateNewTokenService;
import pl.wat.mgr.witowski.ms.security.interceptor.AuthorizationInterceptor;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@Log4j2
@Configuration
@EntityScan("pl.wat.mgr.witowski.ms.database")
@ComponentScan({"pl.wat.mgr.witowski.ms"})
@EnableJpaRepositories("pl.wat.mgr.witowski.ms.database")
public class AppConfig {

    @Autowired
    GenerateNewTokenService generateNewTokenService;

    @PostConstruct
    public void init() {
        try {
            log.debug("send Order To Generate NewKey ");
            generateNewTokenService.sendOrderToGenerateNewKey();
        } catch (Exception ex) {
            log.error("Errorr occured while generate key at startup ", ex);
        }
    }

    @Bean
    public RestTemplate restTemplate() {
        log.debug("Creating bean restTemplate");
        return new RestTemplate();
    }

    @Bean("dictioranyRestTemplare")
    public RestTemplate dictioranyRestTemplare() {
        log.debug("creating bean dictioranyRestTemplare");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new AuthorizationInterceptor());
        return restTemplate;
    }

    @Bean
    @Primary
    public CacheManager cacheManager() {
        log.debug("creating bean cacheManager");
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        GuavaCache dictionaryCache = new GuavaCache("dictionary_cache", CacheBuilder.newBuilder()
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build());
        GuavaCache dictionaryCacheItem = new GuavaCache("dictionary_cache_item", CacheBuilder.newBuilder()
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build());
        cacheManager.setCaches(Arrays.asList( dictionaryCache, dictionaryCacheItem));
        return cacheManager;
    }
}
