package pl.wat.mgr.witowski.ms.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.wat.mgr.witowski.ms.database.dao.IotAgentEntity;

import javax.websocket.server.PathParam;
import java.util.List;

public interface IotAgentEntityRepository extends JpaRepository<IotAgentEntity, String> {

    @Query("select ae from IotAgentEntity ae join ae.iotAgentUsersByIotAgtId aue where" +
            "  ae.iotAgtUid like :agentUid and ae.iotAgtAct = 1 and aue.iotUsrUid like :uuid ")
    IotAgentEntity findAgentByUid(@PathParam("agentUid") String agentUid, @Param("uuid") String uuid);

    @Query("select ae from IotAgentEntity ae join ae.iotAgentUsersByIotAgtId aue where ae.iotAgtAct = 1 and aue.iotUsrUid like :uuid ")
    List<IotAgentEntity> getUserIotAgents(@Param("uuid") String uuid);

    @Query("select iae from IotAgentEntity iae join iae.iotAgentUsersByIotAgtId iaue where iae.iotAgtAct = 1 and iae.iotAgtUid = :agentUid " +
            "and iaue.iotUsrUid = :uuid ")
    IotAgentEntity getUserAgentBasedOnUid(@Param("agentUid") String agentUid, @Param("uuid") String uuid);

    @Query("select iae from IotAgentEntity iae join iae.iotAgentUsersByIotAgtId iaue where iae.iotAgtAct = 1 and iae.iotAgtUid = :agentUid ")
    IotAgentEntity getUserAgentBasedOnUid(@Param("agentUid") String agentUid);


    @Query("select ae from IotAgentEntity ae join ae.iotAgentUsersByIotAgtId aue left  join ae.iotAgentTopicsByIotAgtId at where" +
            "  ae.iotAgtUid like :agentUid and ae.iotAgtAct = 1 and aue.iotUsrUid like :uuid ")
    IotAgentEntity findAgentByUidWithTopics(@PathParam("agentUid") String agentUid, @Param("uuid") String uuid);
}
