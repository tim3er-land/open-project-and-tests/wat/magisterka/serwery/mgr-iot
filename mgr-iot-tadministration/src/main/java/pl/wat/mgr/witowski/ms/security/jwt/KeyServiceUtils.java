package pl.wat.mgr.witowski.ms.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.wat.mgr.witowski.ms.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.ms.dto.security.LoginDto;
import pl.wat.mgr.witowski.ms.dto.security.TokenDto;

@Log4j2
@Component
public class KeyServiceUtils {
    private static String microKey;

    private static String tim3erlandAuthUrl;
    private static String tim3erlandAuthLogin;
    private static String tim3erlandAuthPassword;

    @Value("${pl.wat.mgr.witowski.ms.mgrauth.url}")
    public void setTim3erlandAuthUrl(String tim3erlandAuthUrl) {
        KeyServiceUtils.tim3erlandAuthUrl = tim3erlandAuthUrl + "/login";
    }

    @Value("${pl.wat.mgr.witowski.ms.auth.login}")
    public void settim3erlandAuthLogin(String tim3erlandAuthLogin) {
        KeyServiceUtils.tim3erlandAuthLogin = tim3erlandAuthLogin;
    }

    @Value("${pl.wat.mgr.witowski.ms.auth.password}")
    public void settim3erlandAuthPassword(String tim3erlandAuthPassword) {
        KeyServiceUtils.tim3erlandAuthPassword = tim3erlandAuthPassword;
    }

    public static String refrestMicroToken() {
        KeyServiceUtils.microKey = null;
        return getMicroToken();
    }

    public static String getMicroToken() {
        if (!StringUtils.hasText(KeyServiceUtils.microKey) || !JwtUtils.validateToken(KeyServiceUtils.microKey)) {
            if (!StringUtils.hasText(KeyServiceUtils.tim3erlandAuthUrl))
                return null;
            RestTemplate restTemplate = new RestTemplate();
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(KeyServiceUtils.tim3erlandAuthUrl)
                    // Add query parameter
                    .queryParam("type", "INNER_USER");
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(builder.build().toUri(), LoginDto.builder()
                    .login(KeyServiceUtils.tim3erlandAuthLogin)
                    .password(KeyServiceUtils.tim3erlandAuthPassword)
                    .build(), String.class);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                if (responseEntity.getStatusCode() == HttpStatus.NO_CONTENT) {
                    throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "all-auth-000-001", "all-auth-000-001");
                }
                if (responseEntity.getStatusCode() == HttpStatus.OK) {
                    KeyServiceUtils.microKey = convertJsonToTokenDto(responseEntity.getBody()).getToken();
                }
            } else if (responseEntity.getStatusCode().is5xxServerError()) {
                ApiExceptions apiExceptions = convertJsonToApiExeption(responseEntity.getBody());
                throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "all-auth-000-002", "all-auth-000-001", apiExceptions.getCode());
            }
        }
        return KeyServiceUtils.microKey;
    }

    private static ApiExceptions convertJsonToApiExeption(String body) {
        try {
            return new ObjectMapper().readValue(body, ApiExceptions.class);
        } catch (Exception ex) {
            log.error("Error occured convert json: {}", body);
            return new ApiExceptions();
        }
    }

    private static TokenDto convertJsonToTokenDto(String body) {
        try {
            return new ObjectMapper().readValue(body, TokenDto.class);
        } catch (Exception ex) {
            log.error("Error occured convert json: {}", body);
            return new TokenDto();
        }
    }

    public static String getMicroUid() {
        return JwtUtils.getUUID(KeyServiceUtils.getMicroToken());
    }
}
