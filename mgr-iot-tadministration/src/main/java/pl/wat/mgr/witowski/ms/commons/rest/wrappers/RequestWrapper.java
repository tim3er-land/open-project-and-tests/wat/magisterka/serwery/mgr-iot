package pl.wat.mgr.witowski.ms.commons.rest.wrappers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import lombok.extern.log4j.Log4j2;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import pl.wat.mgr.witowski.ms.commons.rest.CookieLog;
import pl.wat.mgr.witowski.ms.commons.rest.HashMapLog;
import pl.wat.mgr.witowski.ms.commons.rest.OneWayRestApiLog;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
public class RequestWrapper extends HttpServletRequestWrapper {
    OneWayRestApiLog requestLog;
    String body;

    public RequestWrapper(HttpServletRequest request) throws IOException {
        //So that other request method behave just like before
        super(request);

        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;
        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            } else {
                stringBuilder.append("");
            }
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    throw ex;
                }
            }
        }
        body = stringBuilder.toString();
        //Store request pody content in 'body' variable
        HashMap bodyHashMap;
        if (isJSONValid(stringBuilder.toString())) {
            bodyHashMap = new Gson().fromJson(stringBuilder.toString(), HashMap.class);
        } else {
            bodyHashMap = new HashMap();
        }

        requestLog = OneWayRestApiLog.builder()
                .body(bodyHashMap)
                .coockie(prepareCoockie(request.getCookies()))
                .headers(prepareHeaders(request))
                .lenght(StringUtils.hasText(body) ? body.length() : 0)
                .params(prepareParams(request.getParameterMap()))
                .build();
    }

    private MultiValueMap<String, String> prepareParams(Map<String, String[]> parameterMap) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        for (String key : parameterMap.keySet()) {
            for (String value : parameterMap.get(key)) {
                params.add(key, value);
            }
        }
        return params;
    }

    public static boolean isJSONValid(String jsonInString) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.readTree(jsonInString);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private List<CookieLog> prepareCoockie(Cookie[] cookies) {
        if (cookies == null || cookies.length == 0) {
            return Collections.EMPTY_LIST;
        }
        return Arrays.asList(cookies).stream().map(cookie -> CookieLog.builder().name(cookie.getName()).value(cookie.getValue()).build()).collect(Collectors.toList());
    }

    private List<HashMapLog> prepareHeaders(HttpServletRequest request) {
        return Collections.list(request.getHeaderNames())
                .stream().map(s -> {
                    return HashMapLog.builder()
                            .value(request.getHeader(s))
                            .name(s).build();
                }).collect(Collectors.toList());
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(body.getBytes());
        ServletInputStream servletInputStream = new ServletInputStream() {
            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener readListener) {

            }

            public int read() throws IOException {
                return byteArrayInputStream.read();
            }
        };
        return servletInputStream;
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(this.getInputStream()));
    }

    public OneWayRestApiLog getRequestLog() {
        return requestLog;
    }
}
