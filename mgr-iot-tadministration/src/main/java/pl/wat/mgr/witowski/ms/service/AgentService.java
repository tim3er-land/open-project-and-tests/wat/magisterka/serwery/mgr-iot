package pl.wat.mgr.witowski.ms.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import pl.wat.mgr.witowski.ms.commons.MgrUtils;
import pl.wat.mgr.witowski.ms.commons.dictionary.DictionaryCacheService;
import pl.wat.mgr.witowski.ms.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.ms.database.dao.IotAgentEntity;
import pl.wat.mgr.witowski.ms.database.dao.IotAgentRuleEntity;
import pl.wat.mgr.witowski.ms.database.dao.IotAgentTopicEntity;
import pl.wat.mgr.witowski.ms.database.dao.IotAgentUserEntity;
import pl.wat.mgr.witowski.ms.database.repository.IotAgentEntityRepository;
import pl.wat.mgr.witowski.ms.database.repository.IotAgentRuleRepository;
import pl.wat.mgr.witowski.ms.database.repository.IotAgentTopicRepository;
import pl.wat.mgr.witowski.ms.database.repository.IotAgentUserRepository;
import pl.wat.mgr.witowski.ms.dto.*;
import pl.wat.mgr.witowski.ms.dto.rest.AgentExtDto;
import pl.wat.mgr.witowski.ms.dto.rest.BasicAgentDto;
import pl.wat.mgr.witowski.ms.dto.security.FullKeyDto;
import pl.wat.mgr.witowski.ms.security.jwt.JwtUtils;
import pl.wat.mgr.witowski.ms.security.secret.MgrSecretDto;
import pl.wat.mgr.witowski.ms.service.validation.AddEdithAgentRuleValidation;
import pl.wat.mgr.witowski.ms.service.validation.AddNewAgentValidationService;
import pl.wat.mgr.witowski.ms.service.validation.EdithAgentRuleValidation;

import java.io.File;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


@Log4j2
@Service
public class AgentService {

    private final IotAgentEntityRepository iotAgentEntityRepository;
    private final IotAgentUserRepository iotAgentUserRepository;
    private final IotAgentTopicRepository iotAgentTopicRepository;
    private final IotAgentRuleRepository iotAgentRuleRepository;
    private final DictionaryCacheService dictionaryCacheService;
    private final FileService fileService;
    private final KeysServices keysServices;
    private final ObjectMapper objectMapper;

    @Autowired
    public AgentService(IotAgentEntityRepository iotAgentEntityRepository, IotAgentUserRepository iotAgentUserRepository, IotAgentTopicRepository iotAgentTopicRepository, IotAgentRuleRepository iotAgentRuleRepository, DictionaryCacheService dictionaryCacheService, FileService fileService, KeysServices keysServices) {
        this.iotAgentEntityRepository = iotAgentEntityRepository;
        this.iotAgentUserRepository = iotAgentUserRepository;
        this.iotAgentTopicRepository = iotAgentTopicRepository;
        this.iotAgentRuleRepository = iotAgentRuleRepository;
        this.dictionaryCacheService = dictionaryCacheService;
        this.fileService = fileService;
        this.keysServices = keysServices;
        objectMapper = new ObjectMapper();
    }

    public AgentExtDto addNewAgent(BasicAgentDto basicAgentDto) {
        try {
            new AddNewAgentValidationService("mgr-iot-administration-400-001-001"
                    , "mgr-iot-administration-400-001-001",
                    basicAgentDto).validate();
            IotAgentEntity iotAgentEntity = new IotAgentEntity();
            iotAgentEntity.setIotAgtAct((short) 1);
            iotAgentEntity.setIotAgtInsertBy(JwtUtils.getUUID());
            iotAgentEntity.setIotAgtInsertData(LocalDateTime.now());
            iotAgentEntity.setIotAgtUid(MgrUtils.generateNanoId());
            iotAgentEntity.setIotAgtModifyBy(JwtUtils.getUUID());
            iotAgentEntity.setIotAgtModifyData(LocalDateTime.now());
            iotAgentEntity.setIotAgtTypeCode(basicAgentDto.getAgentTypeCode());
            iotAgentEntity.setIotAgtName(basicAgentDto.getAgentName());
            IotAgentEntity save = iotAgentEntityRepository.save(iotAgentEntity);
            iotAgentUserRepository.saveAll(prepareAgentUserCorelaction(save));
            return AgentExtDto.builder()
                    .agentName(save.getIotAgtName())
                    .agentTypeCode(save.getIotAgtTypeCode())
                    .agentUid(save.getIotAgtUid())
                    .build();
        } catch (ApiExceptions ap) {
            throw ap;
        } catch (Exception ex) {
            log.error("Error occured while save new agent", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-500-001-002", "mgr-iot-administration-internal-error");
        }

    }

    private Collection<IotAgentUserEntity> prepareAgentUserCorelaction(IotAgentEntity iotAgentEntity) {
        IotAgentUserEntity iotAgentUserEntity = new IotAgentUserEntity();
        iotAgentUserEntity.setIotAgentByIotAgtId(iotAgentEntity);
        iotAgentUserEntity.setIotUsrUid(JwtUtils.getUUID());
        return Arrays.asList(iotAgentUserEntity);
    }

    public File getAgentZip(String agentUid) {
        try {
            IotAgentEntity agentByUid = iotAgentEntityRepository.findAgentByUid(agentUid, JwtUtils.getUUID());
            if (agentByUid == null) {
                log.warn("Can not find agent: {} ", agentByUid);
                throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-iot-administration-400-002-002", "mgr-iot-administration-400-002-002");
            }
            FullKeyDto signFullKeyDto = keysServices.generateAgentNewKeys();
            FullKeyDto decryptFullKeyDto = keysServices.generateAgentNewKeys();
            agentByUid.setIotAgtPubKeySign(signFullKeyDto.getPubKey());
            agentByUid.setIotAgtPubKeyDecr(decryptFullKeyDto.getPubKey());
            agentByUid.setIotAgtModifyBy(JwtUtils.getUUID());
            agentByUid.setIotAgtModifyData(LocalDateTime.now());
            iotAgentEntityRepository.save(agentByUid);
            MgrSecretDto servicesMgtIotDataPubKey = keysServices.getMgtIotDataPubKey();
            return fileService.prepateZipFile(UUID.randomUUID().toString(), signFullKeyDto, decryptFullKeyDto, servicesMgtIotDataPubKey);
        } catch (ApiExceptions ap) {
            throw ap;
        } catch (Exception ex) {
            log.error("Error occured while get agent zip", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-500-002-001", "mgr-iot-administration-internal-error");
        }
    }

    public List<AgentExtDto> getUserIotAgents() {
        try {
            List<IotAgentEntity> userIotAgents = iotAgentEntityRepository.getUserIotAgents(JwtUtils.getUUID());
            if (CollectionUtils.isEmpty(userIotAgents))
                return null;
            return userIotAgents.parallelStream()
                    .map(iotAgentEntity -> AgentExtDto.builder()
                            .agentUid(iotAgentEntity.getIotAgtUid())
                            .agentName(iotAgentEntity.getIotAgtName())
                            .agentTypeCode(iotAgentEntity.getIotAgtTypeCode())
                            .build())
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("Error occured while getUserIotAgents", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-500-003-001", "mgr-iot-administration-internal-error");
        }
    }

    public void deleteUserAgent(String agentUid) {
        try {
            IotAgentEntity agentForDelete = iotAgentEntityRepository.getUserAgentBasedOnUid(agentUid, JwtUtils.getUUID());
            if (agentForDelete == null) {
                log.warn("Can not find agent: {} for user: {}", agentUid, JwtUtils.getUUID());
                throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-iot-administration-400-003-001", "mgr-iot-administration-400-003-001");
            }
            agentForDelete.setIotAgtAct(MgrUtils.DIS_ACTIVE);
            agentForDelete.setIotAgtModifyData(LocalDateTime.now());
            agentForDelete.setIotAgtModifyBy(JwtUtils.getUUID());
            iotAgentEntityRepository.save(agentForDelete);
        } catch (ApiExceptions ae) {
            throw ae;
        } catch (Exception ex) {
            log.error("Error occured while delete agent: {}", agentUid, ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-500-003-002", "mgr-iot-administration-internal-error");
        }
    }

    public List<AgentTopic> getAgentTopics(String agentUid) {
        try {
            IotAgentEntity agent = iotAgentEntityRepository.getUserAgentBasedOnUid(agentUid, JwtUtils.getUUID());
            if (agent == null) {
                log.warn("Can not find agent: {} for user: {}", agentUid, JwtUtils.getUUID());
                throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-iot-administration-400-004-001", "mgr-iot-administration-400-004-001");
            }
            if (CollectionUtils.isEmpty(agent.getIotAgentTopicsByIotAgtId())) {
                return null;
            }
            return agent.getIotAgentTopicsByIotAgtId().stream().filter(iotAgentTopicEntity -> MgrUtils.ACTIVE.equals(iotAgentTopicEntity.getIotAgtTopAct())).map(iotAgentTopicEntity -> AgentTopic.builder()
                    .topicUid(iotAgentTopicEntity.getIotAgrTopUid())
                    .topicConfig(iotAgentTopicEntity.getIotAgtTopName())
                    .build()).collect(Collectors.toList());
        } catch (ApiExceptions ae) {
            throw ae;
        } catch (Exception ex) {
            log.error("Error occured while delete agent: {}", agentUid, ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-500-004-002", "mgr-iot-administration-internal-error");
        }
    }

    public List<AgentRule> getAgentTopicRules() {
        try {
            List<IotAgentRuleEntity> userRules = iotAgentRuleRepository.getUserRules(JwtUtils.getUUID());
            if (CollectionUtils.isEmpty(userRules)) {
                log.warn("Can not find rules for user: {}", JwtUtils.getUUID());
                return null;
            }
            return userRules.stream().map(iotAgentRuleEntity -> AgentRule.builder()
                    .ruleCode(iotAgentRuleEntity.getIotAgtRulCode())
                    .ruleConfig(iotAgentRuleEntity.getIotAgtRulSetting())
                    .ruleUid(iotAgentRuleEntity.getIotAgtRulUid())
                    .build()).collect(Collectors.toList());
        } catch (ApiExceptions ae) {
            throw ae;
        } catch (Exception ex) {
            log.error("Error occured while get user rules: {}", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-005-002", "mgr-iot-administration-005-002");
        }
    }

    public void modifyAgentRule(String ruleUid, AgentRuleBasic agentRuleBasic) {
        try {
            IotAgentRuleEntity userRule = iotAgentRuleRepository.getUserRule(ruleUid, JwtUtils.getUUID());
            if (userRule == null) {
                log.warn("Can not find rule: {} for user: {}", ruleUid, JwtUtils.getUUID());
                throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-iot-administration-005-001", "mgr-iot-administration-005-001");
            }
            new AddEdithAgentRuleValidation("mgr-iot-administration-005-002", "mgr-iot-administration-005-002"
                    , dictionaryCacheService.getDictionaryItems("RULES"), agentRuleBasic, true).validate();
            userRule.setIotAgtRulCode(agentRuleBasic.getRuleCode());
            userRule.setIotAgtRulSetting(objectMapper.writeValueAsString(agentRuleBasic.getRuleConfig()));
            userRule.setIotAgtRulModifyBy(JwtUtils.getUUID());
            userRule.setIotAgtRulModifyDate(LocalDateTime.now());
            iotAgentRuleRepository.save(userRule);
        } catch (ApiExceptions ae) {
            throw ae;
        } catch (Exception ex) {
            log.error("Error occured while modifyAgentRule", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-005-003", "mgr-iot-administration-005-003");
        }
    }

    public void addAgentRule(AgentRuleBasic agentRuleBasic) {
        try {
            new AddEdithAgentRuleValidation("mgr-iot-administration-006-002", "mgr-iot-administration-006-002"
                    , dictionaryCacheService.getDictionaryItems("RULES"), agentRuleBasic, false).validate();
            IotAgentRuleEntity iotAgentRule = new IotAgentRuleEntity();
            iotAgentRule.setIotAgtRulInsertBy(JwtUtils.getUUID());
            iotAgentRule.setIotAgtRulUid(UUID.randomUUID().toString());
            iotAgentRule.setIotAgtRulInsertDate(LocalDateTime.now());
            iotAgentRule.setIotAgtRulAct(MgrUtils.ACTIVE);
            iotAgentRule.setIotUsrUid(JwtUtils.getUUID());
            iotAgentRule.setIotAgtRulCode(agentRuleBasic.getRuleCode());
            iotAgentRule.setIotAgtRulSetting(objectMapper.writeValueAsString(agentRuleBasic.getRuleConfig()));
            iotAgentRule.setIotAgtRulModifyBy(JwtUtils.getUUID());
            iotAgentRule.setIotAgtRulModifyDate(LocalDateTime.now());
            iotAgentRuleRepository.save(iotAgentRule);
        } catch (ApiExceptions ae) {
            throw ae;
        } catch (Exception ex) {
            log.error("Error occured while add new agent rule ", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-006-003", "mgr-iot-administration-006-003");
        }

    }

    public void deleteAgentRule(String ruleUid) {
        try {
            IotAgentRuleEntity iotAgentRule = iotAgentRuleRepository.getUserRule(ruleUid, JwtUtils.getUUID());
            if (iotAgentRule == null) {
                log.warn("Can not find rule: {} for user: {}", ruleUid, JwtUtils.getUUID());
                throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-iot-administration-400-007-001", "mgr-iot-administration-400-007-001");
            }
            iotAgentRule.setIotAgtRulAct(MgrUtils.DIS_ACTIVE);
            iotAgentRule.setIotAgtRulModifyBy(JwtUtils.getUUID());
            iotAgentRule.setIotAgtRulModifyDate(LocalDateTime.now());
            iotAgentRuleRepository.save(iotAgentRule);
        } catch (ApiExceptions ae) {
            throw ae;
        } catch (Exception ex) {
            log.error("Error occured while delete rule: {}", ruleUid, ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-500-007-003", "mgr-iot-administration-internal-error");
        }
    }

    public void addAgentTopic(String agentUid, List<String> topics) {
        try {
            Optional<IotAgentEntity> agent = Optional.ofNullable(iotAgentEntityRepository.getUserAgentBasedOnUid(agentUid));
            if (!agent.isPresent()) {
                log.warn("Can not find agent: {}", agentUid);
                throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-iot-administration-400-008-001", "mgr-iot-administration-400-008-001");
            }
            List<IotAgentTopicEntity> topicList = topics.stream().map(topic -> {
                IotAgentTopicEntity agentTopic = new IotAgentTopicEntity();
                agentTopic.setIotAgtTopAct(MgrUtils.ACTIVE);
                agentTopic.setIotAgtTopName(topic);
                agentTopic.setIotAgrTopUid(UUID.randomUUID().toString());
                agentTopic.setIotAgtTopModifyBy(JwtUtils.getUUID());
                agentTopic.setIotAgtTopInsertBy(JwtUtils.getUUID());
                agentTopic.setIotAgtTopModifyDate(LocalDateTime.now());
                agentTopic.setIotAgtTopInsertDate(LocalDateTime.now());
                agentTopic.setIotAgentByIotAgtId(agent.get());
                return agentTopic;
            }).collect(Collectors.toList());
            iotAgentTopicRepository.saveAll(topicList);
        } catch (ApiExceptions ae) {
            throw ae;
        } catch (Exception ex) {
            log.error("Error occured while delete agent: {}", agentUid, ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-500-008-002", "mgr-iot-administration-internal-error");
        }
    }

//    public void modifyAgentTopic(String agentUid, String topicUid, String topic) {
//        try {
//            IotAgentEntity agent = iotAgentEntityRepository.getUserAgentBasedOnUid(agentUid, JwtUtils.getUUID());
//            if (agent == null) {
//                log.warn("Can not find agent: {} for user: {}", agentUid, JwtUtils.getUUID());
//                throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-iot-administration-009-001", "mgr-iot-administration-009-001");
//            }
//            IotAgentTopicEntity agentTopic = iotAgentTopicRepository.findTopicByUid(topicUid, agent);
//            if (agentTopic == null) {
//                log.warn("Can not find agent: {} for user: {}", agentUid, JwtUtils.getUUID());
//                throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-iot-administration-009-002", "mgr-iot-administration-009-002");
//            }
//            agentTopic.setIotAgtTopModifyDate(LocalDateTime.now());
//            agentTopic.setIotAgtTopModifyBy(JwtUtils.getUUID());
//            agentTopic.setIotAgtTopName(topic);
//            iotAgentTopicRepository.save(agentTopic);
//        } catch (ApiExceptions ae) {
//            throw ae;
//        } catch (Exception ex) {
//            log.error("Error occured while add new agent rule agent: {}", agentUid, ex);
//            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-009-003", "mgr-iot-administration-009-003");
//        }
//    }

    public void deleteAgentTopic(String agentUid) {
        try {
            Optional<IotAgentEntity> agent = Optional.ofNullable(iotAgentEntityRepository.getUserAgentBasedOnUid(agentUid));
            if (!agent.isPresent()) {
                log.warn("Can not find agent: {}", agentUid);
                throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-iot-administration-400-010-001", "mgr-iot-administration-400-010-001");
            }
            List<IotAgentTopicEntity> agentTopics = iotAgentTopicRepository.findTopicsByUid(agent.get());
            if (CollectionUtils.isEmpty(agentTopics)) {
                log.warn("Can not find agent: {} for user: {}", agentUid, JwtUtils.getUUID());
                return;
            }
            agentTopics.stream().forEach(agentTopic -> {
                agentTopic.setIotAgtTopModifyDate(LocalDateTime.now());
                agentTopic.setIotAgtTopModifyBy(JwtUtils.getUUID());
                agentTopic.setIotAgtTopAct(MgrUtils.DIS_ACTIVE);
            });
            iotAgentTopicRepository.saveAll(agentTopics);
        } catch (ApiExceptions ae) {
            throw ae;
        } catch (Exception ex) {
            log.error("Error occured while add new agent rule agent: {}", agentUid, ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-500-010-002", "mgr-iot-administration-internal-error");
        }
    }

    public AgentInfoDto getAgentInfo(String agentUid) {
        try {
            IotAgentEntity agentByUid = iotAgentEntityRepository.findAgentByUidWithTopics(agentUid, JwtUtils.getUUID());
            if (agentByUid == null) {
                log.warn("Can not find agent: {} ", agentByUid);
                throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-iot-administration-400-011-001", "mgr-iot-administration-400-011-001");
            }
            return AgentInfoDto.builder()
                    .agentName(agentByUid.getIotAgtName())
                    .agentUid(agentByUid.getIotAgtTypeCode())
                    .agentModifyDate(agentByUid.getIotAgtModifyData().toString())
                    .agentTopics(agentByUid.getIotAgentTopicsByIotAgtId().stream().map(IotAgentTopicEntity::getIotAgtTopName).collect(Collectors.toList()))
                    .build();
        } catch (ApiExceptions apiExceptions) {
            throw apiExceptions;
        } catch (Exception ex) {
            log.error("Error occured while getAgentInfo for agent: {}", agentUid, ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-administration-500-011-001", "mgr-iot-administration-internal-error");
        }
    }
}
