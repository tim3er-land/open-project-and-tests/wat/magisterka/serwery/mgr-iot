package pl.wat.mgr.witowski.ms.service.validation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import pl.wat.mgr.witowski.ms.commons.BasicValidationServices;
import pl.wat.mgr.witowski.ms.dto.dict.DictionaryItemDto;
import pl.wat.mgr.witowski.ms.dto.rest.BasicAgentDto;

import java.util.List;

@Slf4j
public class AddNewAgentValidationService extends BasicValidationServices {
    private BasicAgentDto basicAgentDto;

    public AddNewAgentValidationService(String errorCode, String errorDescription, BasicAgentDto basicAgentDto) {
        super(errorCode, errorDescription);
        this.basicAgentDto = basicAgentDto;
    }

    @Override
    public void validate() {
        if (!StringUtils.hasText(basicAgentDto.getAgentName())) {
            log.warn("Agent must have name");
            addNewErrorDto("name", "mgr-iot-administration-001-001", "mgr-iot-administration-001-001");
        }
        super.validate();
    }
}
