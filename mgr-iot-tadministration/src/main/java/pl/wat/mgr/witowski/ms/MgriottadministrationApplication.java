package pl.wat.mgr.witowski.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MgriottadministrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(MgriottadministrationApplication.class, args);
    }

}
